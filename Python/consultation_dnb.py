import explore_dnb as dnb
import os


#Création de variables globales pour changer la couleur d'affichage
green = '\033[92m'
red = '\033[91m' #A ENLEVER
white = '\033[0m'


# Ici vos fonctions dédiées aux interactions
def afficher_menu(titre, liste_options):
    """Affiche le menu que l'on désire

    Args:
        titre (str): le titre du menu de l'application
        liste_options (lst): une liste d'options pour ce menu sous forme de str
    """    
    ligne = ''
    for ind_titre in range(len(titre)+4):
        if ind_titre == 0 or ind_titre == len(titre)+3:
            ligne += '+'
        else:
            ligne += '-'
    print(red, ligne, white)
    print(red, "| "+ titre+ " |", white)
    print(red, ligne, white)
        
    for ind in range(len(liste_options)):
        print(str(ind+1)+" -> "+green, liste_options[ind], white)


def demander_nombre(message, borne_max):
    """Teste si le nombre entré par l'utilisateur est compris entre 1 et borne_max

    Args:
        message (str): le message que l'on veut afficher à l'utilisateur
        borne_max (int): la borne max désignant la dernière option du menu

    Returns:
        (int): l'entier désignant l'option choisi par l'utilisateur
    """    
    choix = input(message)
    if choix.isdecimal():
        choix = int(choix)
        if choix >=1 and choix <= borne_max:
            return choix
    return None

def menu(titre, liste_options):
    """Retourne l'option du menu choisie par l'utilisateur ou None si la val est incrorrecte

    Args:
        titre (str): le titre du menu
        liste_options (lst): la liste d'options sous forme de str

    Returns:
        (int): l'entier désignant l'option que l'utilisateur a choisi
    """
    afficher_menu(titre, liste_options)
    return demander_nombre("Entrez votre choix [1-"+str(len(liste_options))+"]: ", len(liste_options))

def nom_departement(departement):
    """Connaître la signification d'un numéro de département

    Args:
        departement (int): le numéro de département
        
    Returns:
        (str): la chaine de caractere désignant le nom du département
    """
    liste_noms_départements = ["Ain", "Aisne", "Allier", "Alpes-de-Haute-Provence", "Hautes-Alpes", "Alpes-Maritimes", "Ardèche", "Ardennes", "Ariège", "Aube", "Aude", "Aveyron",
                               "Bouches-du-Rhône", "Calvados", "Cantal", "Charente", "Charente-Maritime", "Cher", "Corrèze", "Corse", "Côte-d'Or", "Cotes d'Armor", "Creuse",
                               "Dordogne", "Doubs", "Drôme", "Eure", "Eure-et-Loir", "Finistère", "Gard", "Haute-Garonne", "Gers", "Gironde", "Hérault", "Ille-et-Vilaine",
                               "Indre", "Indre-et-Loire", "Isère", "Jura", "Landes", "Loir-et-Cher", "Loire", "Haute-Loire", "Loire-Atlantique", "Loiret", "Lot",
                               "Lot-et-Garonne", "Lozère", "Maine-et-Loire", "Manche", "Marne", "Haute-Marne", "Mayenne", "Meurthe-et-Moselle", "Meuse", "Morbihan", "Moselle",
                               "Nièvre", "Nord", "Oise", "Orne", "Pas-de-Calais", "Puy-de-Dôme", "Pyrénées-Atlantiques", "Hautes-Pyrénées", "Pyrénées Orientales", "Bas-Rhin",
                               "Haut-Rhin", "Rhône", "Haute-Saône", "Saône-et-Loire", "Sarthe", "Savoie", "Haute-Savoie", "Paris", "Seine-Maritime", "Seine-et-Marne",
                               "Yvelines", "Deux-Sèvres", "Somme", "Tarn", "Tarn-et-Garonne", "Var", "Vaucluse", "Vendée", "Vienne", "Haute-Vienne", "Vosges", "Yonne",
                               "Territoire de Belfort", "Essonne", "Hauts-de-Seine", "Seine-Saint-Denis", "Val-de-Marne", "Val-d'Oise", "Guadeloupe", "Martinitique", "Guyane",
                               "La Réunion", "Mayotte", "Inconnu", "Inconnu"] #le département 101 et 102 n'existe pas réelement donc j'ai mit Inconnu
    return liste_noms_départements[departement-1]


# ici votre programme principal
def programme_principal():
    """Crée un menu d'application et affiche à chaque fois l'option choisie par l'utilisateur jusqu'à ce que l'utilisateur choisisse l'option 11 (quitter)
    """
    liste_options = ["Charger un fichier pour le DNB",
                     "Calculer taux de réussite d'un collège sur une session au DNB",
                     "Calculer taux de réussite global sur une session au DNB",
                     "Savoir le total d'admis et de présents pour un collège sur une session",
                     "Calculer la moyenne des taux de réussite d'un collège",
                     "Savoir quel est le collège de France qui a eu le meilleur taux de réussite et celui qui a eu le pire taux de réussite au DNB",
                     "Savoir le collège d'un département sur une session qui a eu le meilleur taux de réussite au DNB",
                     "Savoir la plus longue période d'amélioration du taux national de réussite au DNB",
                     "Calculer le taux de réussite moyen d'un collège sur une période choisie",
                     "Savoir les collèges dont le taux de réussite est en baisse sur les nb_sessions dernières sessions",
                     "Quitter"]

    liste_resultats = []

    while True:
        os.system("clear") #rafraichir le terminal pour rendre plus esthétique l'affichage
        rep = menu("MENU DE L'APPLICATION POUR LE DNB", liste_options)

        if rep is None:
            print("Cette option n'existe pas")
        elif rep == 1:
            print("Vous avez choisi",green,liste_options[rep - 1],white)
            choix_fic = input("Entrez le nom du fichier: ")
            liste_resultats = dnb.charger_resultats(choix_fic)
        elif rep == 2:
            print("Vous avez choisi",green,liste_options[rep - 1],white)
            choix_departement = input("Entrez le département: ")
            choix_nom = input("Entrez le nom: ")
            choix_session = input("Entrez la session: ")
            try:
                sous_liste_resultats_college = dnb.filtre_college(liste_resultats, choix_nom, int(choix_departement))
                resultat_recherche = dnb.filtre_session(sous_liste_resultats_college, int(choix_session))
                taux_reussite_recherche = dnb.taux_reussite(resultat_recherche[0])
                nom_choix_departement = nom_departement(int(choix_departement))
                print("Le taux de réussite du collège "+choix_nom+" du département "+nom_choix_departement+" ("+choix_departement+") pour la session de "+choix_session+" était de "+str(round(taux_reussite_recherche, 2))+"%")
                classement = dnb.classement_global_departement(liste_resultats, int(choix_departement))
                ind_place = 0
                for place_classement in classement:
                    if place_classement[0] == choix_nom:
                        print("Le collège est classé "+str(ind_place)+"ème dans le département "+nom_choix_departement+" ("+str(choix_departement)+") sur toutes les sessions confondues")
                    else:
                        ind_place += 1
            except:
                print("Aucun fichier n'a été chargé ou il n'y a aucun résultat pour le collège et la session choisi")
        elif rep == 3:
            print("Vous avez choisi",green,liste_options[rep - 1],white)
            choix_session = input("Entrez la session: ")
            try:
                taux_reussite_global_session = dnb.taux_reussite_global(liste_resultats, int(choix_session))
                print("Le taux de réussite global pour la session de "+str(choix_session)+" était de "+str(round(taux_reussite_global_session, 2))+"%")
            except:
                print("Aucun fichier n'a été chargé ou il n'y a aucun résultat pour la session choisie")
        elif rep == 4:
            print("Vous avez choisi",green,liste_options[rep - 1],white)
            choix_session = input("Entrez la session: ")
            try:
                sous_liste_resultats_session = dnb.filtre_session(liste_resultats, int(choix_session))
                tuple_admis_presents = dnb.total_admis_presents(sous_liste_resultats_session)
                print("Pour la session de "+choix_session+" il y avait "+str(tuple_admis_presents[0])+" admis parmi "+str(tuple_admis_presents[1])+" présents au DNB")
                if tuple_admis_presents[1] > 0:
                    choix_plus_precis = input("Souhaitez-vous savoir combien un département a eu de candidats présents et admis au DNB ? (O ou N): ")
                    if choix_plus_precis == 'o' or choix_plus_precis == 'O':
                        choix_departement = input("Entrez le nom du département: ")
                        try:
                            sous_liste_departements = dnb.filtre_departement(sous_liste_resultats_session, int(choix_departement))
                            tuple_admis_presents = dnb.total_admis_presents(sous_liste_departements)
                            nom_choix_departement = nom_departement(int(choix_departement))
                            print("Le département "+nom_choix_departement+" ("+choix_departement+") a présenté "+str(tuple_admis_presents[1])+" candidats au DNB de "+choix_session)
                            if tuple_admis_presents[1] > 0:
                                choix_plus_precis = input("Souhaitez-vous savoir combien un collège du département a eu de candidats présents et admis au DNB ? (O ou N): ")
                                if choix_plus_precis == 'o' or choix_plus_precis == 'O':
                                    choix_nom = input("Entrez le nom du collège: ")
                                    try:
                                        sous_liste_resultats_college = dnb.filtre_college(sous_liste_departements, choix_nom, int(choix_departement))
                                        if len(sous_liste_resultats_college) != len(sous_liste_departements): #si on a bien entré une valeur pour le nom
                                            tuple_admis_presents_college = dnb.total_admis_presents(sous_liste_resultats_college)
                                            print("Le collège "+choix_nom+" du département "+nom_choix_departement+" ("+choix_departement+") a présenté "+str(tuple_admis_presents_college[1])+" candidats au DNB de "+choix_session)
                                    except:
                                        print("Erreur de valeur entrée !")
                        except:
                            print("Erreur de valeur entrée !")
            except:
                print("Aucun fichier n'a été chargé ou il y a une erreur dans les valeurs entrées")
        elif rep == 5:
            print("Vous avez choisi",green,liste_options[rep - 1],white)
            choix_nom = input("Entrez le nom: ")
            choix_departement = input("Entrez le département: ")
            try:
                moyenne_taux_reussite_recherche = dnb.moyenne_taux_reussite_college(liste_resultats, choix_nom, int(choix_departement))
                nom_choix_departement = nom_departement(int(choix_departement))
                print("Le collège "+choix_nom+" dans le département "+nom_choix_departement+" ("+choix_departement+") a une moyenne de taux de réussite de "+str(round(moyenne_taux_reussite_recherche, 2))+"%")
            except:
                print("Aucun fichier n'a été chargé ou il n'y a aucun résultat pour le collège choisi")
        elif rep == 6:
            print("Vous avez choisi",green,liste_options[rep - 1],white)
            try:
                meilleur_taux_reussite_recherche = dnb.meilleur_taux_reussite(liste_resultats)
                pire_taux_reussite_recherche = dnb.pire_taux_reussite(liste_resultats)
                print("Le collège de France qui a eu le meilleur taux de réussite est de "+str(round(meilleur_taux_reussite_recherche, 2))+"% et celui qui a eu le pire taux de réussite au DNB est de "+str(round(pire_taux_reussite_recherche, 2))+"%")
            except:
               print("Aucun fichier n'a été chargé ou les résultats ont tous des erreurs")
        elif rep == 7:
            print("Vous avez choisi",green,liste_options[rep - 1],white)
            choix_departement = input("Entrez le numéro de département: ")
            choix_session = input("Entrez la session: ")
            try:
                sous_liste_departements = dnb.filtre_departement(liste_resultats, int(choix_departement))
                meilleur_college_recherche = dnb.meilleur_college(sous_liste_departements, int(choix_session))
                nom_choix_departement = nom_departement(int(choix_departement))
                print("Le collège qui a eu le meilleur taux de réussite dans le département "+nom_choix_departement+" ("+choix_departement+") pour la session de "+choix_session+" était "+meilleur_college_recherche[0])
                moyenne_taux_reussite_recherche = dnb.moyenne_taux_reussite_college(liste_resultats, meilleur_college_recherche[0], meilleur_college_recherche[1])
                est_trouve = False
                ind = 0
                while not est_trouve: #pour savoir la première session où le collège choisi a un résultat
                    if liste_resultats[ind][1] == meilleur_college_recherche[0] and liste_resultats[ind][2] == meilleur_college_recherche[1]:
                        est_trouve = True
                    else:
                        ind += 1
                print("Depuis "+str(liste_resultats[ind][0])+" son taux moyen de réussite est de "+str(round(moyenne_taux_reussite_recherche))+"%")
            except:
                print("Il y a une erreur de fichier chargé ou de valeurs entrées")
        elif rep == 8:
            print("Vous avez choisi",green,liste_options[rep - 1],white)
            max_periode_amelioration = dnb.plus_longue_periode_amelioration(liste_resultats)
            if max_periode_amelioration != (None, None):
                print("La plus longue période d'amélioration du taux national de réussite au DNB a commencé en "+str(max_periode_amelioration[0])+" et c'est fini en "+str(max_periode_amelioration[1]))
            else:
                print("Aucun fichier n'a été chargé ou les résultats ont tous des erreurs")
        elif rep == 9:
            print("Vous avez choisi",green,liste_options[rep - 1],white)
            choix_nom = input("Entrez le nom: ")
            choix_departement = input("Entrez le département: ")
            choix_periode_debut = input("Entrez la période de début: ")
            choix_periode_fin = input("Entrez la période de fin: ")
            try:
                taux_moyen_periode = dnb.taux_moyen_college_periode(liste_resultats, choix_nom, int(choix_departement), (int(choix_periode_debut), int(choix_periode_fin)))
                nom_choix_departement = nom_departement(int(choix_departement))
                print("Le taux de réussite moyen du collège "+choix_nom+" dans le département "+nom_choix_departement+" ("+choix_departement+") sur la période ("+choix_periode_debut+"-"+choix_periode_fin+") est de "+str(round(taux_moyen_periode))+"%")
            except:
                print("Aucun fichier n'a été chargé ou il n'y a aucun résultat pour le collège et la période choisi")
        elif rep == 10: #PROBLEME A REVOIR -> float division by zero
            print("Vous avez choisi",green,liste_options[rep - 1],white)
            choix_dernieres_sessions = input("Entrez sur les combiens de dernières sessions: ")
            liste_colleges_en_baisse = dnb.colleges_en_baisse(liste_resultats, int(choix_dernieres_sessions))
            if liste_colleges_en_baisse != []:
                for college in liste_colleges_en_baisse:
                    evolution = college[2] * (-1) #pour avoir un affichage propre du pourcentage de baisse du taux de réussite
                    print("Le taux de réussite au DNB du collège "+str(college[0])+" dans le département ("+str(college[1])+") est en baisse de "+str(evolution)+"% sur les "+choix_dernieres_sessions+" dernières sessions")
            else:
                print("Aucun fichier n'a été chargé ou il n'y a aucun collège dont le taux de réussite au DNB est en baisse sur les "+choix_dernieres_sessions+" dernières sessions")

        #ECRIRE DES INFOS EN PLUS POUR CHAQUE OPTION (VOIR LE PDF DE LA SAE)
        #AJOUTER DES OPTIONS POUR RÉPONDRE AUX QUESTIONS DU PDF DE LA SAE

        #IL RESTE A FAIRE -> PROBLEME option 10 + NOUVELLE OPTION POUR CHARGER LES 3 FICHIERS DNB.CSV ??
        # + POUR LES DERNIÈRES FONCTIONS ÉCRITES TESTER AVEC DES RÉSULTATS INVALIDES (<0, nb_admis > nb_present)
        # + FAIRE DES TESTS OÙ TT EST VIDE (liste et dept ou nom par exemple)

        # CHANGER OPTION 4 POUR CHOISIR total tt court, total sur un dept, total pour un college (voir point 3)
        # IMPLÉMENTER UNE NEW OPTION POUR SAVOIR QUEL EST LE COLLEGE QUI A EU LE PIRE TAUX DE RÉUSSITE ET LE MEILLEUR TANT QU'ON Y EST

        else:
            break
        input("Appuyer sur Entrée pour continuer ")
    print("Merci au revoir !")

programme_principal()