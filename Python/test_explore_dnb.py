import explore_dnb as dnb

# -----------------------------------------------------------------------------------------------------
# fonctions de tests à compléter
# -----------------------------------------------------------------------------------------------------

def test_taux_reussite():
    assert dnb.taux_reussite(dnb.resultat1) == 67/71*100 #cas normal
    assert dnb.taux_reussite(dnb.resultat2) == 78/98*100 #cas normal
    assert dnb.taux_reussite(()) is None #cas où le résultat est vide
    assert dnb.taux_reussite((2008, "ANATOLE FRANCE", 37, 42, 53)) is None #cas où le nombre d'élèves admis est supérieur au nombre d'élèves qui se sont présentés au DNB
    assert dnb.taux_reussite((2008, "ANATOLE FRANCE", 37, 0, 71)) is None #cas où aucun élèves ne se sont présentés au DNB
    assert dnb.taux_reussite((2008, "ANATOLE FRANCE", 37, 45, 0)) == 0 #cas où aucun élève n'a été admis au DNB
    assert dnb.taux_reussite((2012, "JEAN MONNET", 45, 79, -5)) is None #cas où le nombre d'élèves admis est négatif
    assert dnb.taux_reussite((2012, "JEAN MONNET", 45, -13, 45)) is None #cas où le nombre d'élèves présents au DNB est négatif       

           
def test_meilleur():
    assert dnb.meilleur(dnb.resultat1, dnb.resultat2) #cas normal
    assert not dnb.meilleur(dnb.resultat1, dnb.resultat3) #cas normal
    assert dnb.meilleur(dnb.resultat1, ()) is None #cas où le deuxième résultat est vide
    assert dnb.meilleur((), dnb.resultat2) is None #cas où le premier résultat est vide
    assert not dnb.meilleur(dnb.resultat1, dnb.resultat1) #cas où les deux résultats sont égaux
    assert dnb.meilleur((2012, "JEAN MONNET", 45, -13, 45), dnb.resultat2) is None #cas où un résultat a une erreur (retourne None car le nombre de présents est négatifs soit impossible)


def test_meilleur_taux_reussite():
    assert dnb.meilleur_taux_reussite(dnb.liste2) == 27/28*100 #cas normal
    assert dnb.meilleur_taux_reussite(dnb.liste3) == 1.0*100 #cas normal
    assert dnb.meilleur_taux_reussite([(2020, 'ALBERT SIDOISNE', 43, 0, 71), (2020, 'ANATOLE FRANCE', 12, 63, 47), (2020, 'DE NERMONT - CHATEAUDUN', 68, 74, 60)]) == 60/74*100 #cas où le premier
    # résultat de la liste a une erreur (retourne None) car ne nb_présents est de 0 alors que le nb_admis est de 71
    assert dnb.meilleur_taux_reussite([(2012, 'DE NERMONT - CHATEAUDUN', 37, 134, 118), (2020, 'ANATOLE FRANCE', 38, 63, 78), (2022, 'MATHURIN REGNIER', 78, 142, -45)]) == 118/134*100 #cas où un résultat
    # de la liste (sans aucune importance de place) a une erreur (retourne None) car pour un résultat le nb_admis est supérieur au nb_présents et pour un autre le nb_admis est négatif
    assert dnb.meilleur_taux_reussite([(2021, 'ALBERT SIDOISNE', 14, 134, 183), (2023, 'MATHURIN REGNIER', 35, -15, 48)]) is None #cas où tous les résultats de la liste ont des erreurs (retourne None)
    #car un résultat a un nb_admis supérieur au nb_présents et l'autre a un nb_présents négatif


def test_pire_taux_reussite():
    assert dnb.pire_taux_reussite(dnb.liste1) == 47/63*100 #cas normal
    assert dnb.pire_taux_reussite(dnb.liste2) == 15/24*100 #cas normal
    assert dnb.pire_taux_reussite([(2011, 'ALBERT SIDOISNE', 42, 134, 183), (2018, 'ANATOLE FRANCE', 35, 78, -45)]) is None #cas où tous les résultats de la liste ont des erreurs (retourne None)
    assert dnb.pire_taux_reussite([(2000, 'ALBERT SIDOISNE', 43, 0, 41), (2014, 'ANATOLE FRANCE', 43, 45, 32), (2021, 'DE NERMONT - CHATEAUDUN', 18, 74, 73)]) == 32/45*100 #cas où le premier
    # résultat de la liste a une erreur (retourne None)
    assert dnb.pire_taux_reussite([(2011, 'ALBERT SIDOISNE', 37, 130, 96), (2016, 'ANATOLE FRANCE', 31, 63, -45), (2022, 'MATHURIN REGNIER', 78, 142, 134)]) == 96/130*100 #cas où un résultat
    # de la liste (sans aucune importance de place) a une erreur (retourne None)


def test_total_admis_presents():
    assert dnb.total_admis_presents(dnb.liste1) == (476, 576) #cas normal
    assert dnb.total_admis_presents(dnb.liste2) == (922, 1111) #cas normal
    assert dnb.total_admis_presents([]) == (0, 0) #cas où la liste est vide
    assert dnb.total_admis_presents([(2020, 'ALBERT SIDOISNE', 14, 130, 110), (2022, 'ALBERT SIDOISNE', 37, 120, 180), (2024, 'MATHURIN REGNIER', 75, 150, 90),
                                     (2031, 'ANATOLE FRANCE', 35, 78, -45)]) == (200, 280) #cas où on ne compte pas le résultat quand il y a une erreur car le nombre d'admis est supérieur au nombre de présents
    assert dnb.total_admis_presents([(2020, 'ALBERT SIDOISNE', 44, 142, 175), (2021, 'ALBERT SIDOISNE', 36, -45, 14), (2013, 'MATHURIN REGNIER', 13, 700, -452)]) == (0, 0) #cas où tous les résultats
    # ont des erreurs car le nombre d'élèves admis supérieur au nombre de présents ou le nb_admis ou nb_présents est négatif au DNB


def test_filtre_session():
    assert dnb.filtre_session(dnb.liste4, 2020) == [(2020, 'ALBERT SIDOISNE', 28, 134, 118), (2020, 'MATHURIN REGNIER', 28, 152, 118)] #cas normal
    assert dnb.filtre_session(dnb.liste4, 2018) == [] #cas où les résultats de la liste n'ont pas la session demandée
    assert dnb.filtre_session([], 2022) == [] #cas où la liste de résultats est vide


def test_filtre_departement():
    assert dnb.filtre_departement(dnb.liste1, 45) == [] #cas où les résultats de la liste n'ont pas le département demandée
    assert dnb.filtre_departement(dnb.liste4, 28) == [(2012, "ALBERT SIDOISNE", 28, 98, 78),(2020, 'ALBERT SIDOISNE', 28, 134, 118),(2020, 'MATHURIN REGNIER', 28, 152, 118)] #cas normal
    assert dnb.filtre_departement([], 21) == [] #cas où la liste est vide
    assert dnb.filtre_departement([(2019, "ALBERT SIDOISNE", 18, 90, 26), (2022, 'ALBERT SIDOISNE', 15, 0, 118)], -18) == [] #cas où le département est négatif
    assert dnb.filtre_departement([(2019, "ALBERT SIDOISNE", 18, 90, 26), (2022, 'ALBERT SIDOISNE', 86, 0, 118)], 105) == [] #cas où le département est supérieur à 101 (donc impossible)


def test_filtre_college():
    assert dnb.filtre_college(dnb.liste1, 'EMILE', 45) == [] #cas où tous les résultats n'ont pas le département demandé
    assert dnb.filtre_college(dnb.liste1, 'SARTRE', 28) == [] #cas où tous les résultats ont le département demandé mais pas le nom demandé
    assert dnb.filtre_college(dnb.liste1, 'SARTRE', 45) == [] #cas où les résultats n'ont ni le département demandé ni le nom demandé
    assert dnb.filtre_college(dnb.liste1, 'NERMONT', 28) == [(2020, 'DE NERMONT - CHATEAUDUN', 28, 74, 60),(2020, 'DE NERMONT - NOGENT', 28, 28, 27)] #cas où on passe le début du nom en majuscules
    assert dnb.filtre_college(dnb.liste1, 'nermont', 28) == [(2020, 'DE NERMONT - CHATEAUDUN', 28, 74, 60),(2020, 'DE NERMONT - NOGENT', 28, 28, 27)] #cas où le début du nom est en minuscules
    assert dnb.filtre_college(dnb.liste1, 'nErmONT', 28) == [(2020, 'DE NERMONT - CHATEAUDUN', 28, 74, 60),(2020, 'DE NERMONT - NOGENT', 28, 28, 27)] #cas où il y a majuscules et minuscules dans le nom
    assert dnb.filtre_college(dnb.liste1, 'DE NERMONT - NOGENT', 28) == [(2020, 'DE NERMONT - NOGENT', 28, 28, 27)] #cas normal (le nom passé en entier et en majuscules)


def test_taux_reussite_global():
    assert dnb.taux_reussite_global(dnb.liste1, 2018) is None #cas où il y aucun résultat de la session demandée
    assert dnb.taux_reussite_global([(2020, "ALBERT SIDOISNE", 18, 90, -26), (2020, 'ALBERT SIDOISNE', 14, 0, 118)], 2020) is None #cas où il n'y que des erreurs lors de la session demandée
    assert dnb.taux_reussite_global(dnb.liste1, 2020) == 476/576*100 #cas où il n'y que des résultats de la session demandée
    assert dnb.taux_reussite_global([(2020, "ALBERT SIDOISNE", 18, 90, 68), (2020, 'ALBERT SIDOISNE', 35, 0, 45)], 2020) == 68/90*100 #cas où il y au moins une erreur lors de la session demandée
    # (on ne la compte pas du coup)
    assert dnb.taux_reussite_global(dnb.liste4, 2020) == (118+118)/(134+152)*100 #cas normal


def test_moyenne_taux_reussite_college():
    assert dnb.moyenne_taux_reussite_college(dnb.liste1, 'ALBERT SIDOISNE', 28) == 118/134*100 #cas normal
    assert dnb.moyenne_taux_reussite_college(dnb.liste2, 'GILBERT COURTOIS', 28) == (18/22*100+15/24*100)/2 #cas normal
    assert dnb.moyenne_taux_reussite_college([(2021, 'ALBERT SIDOISNE', 44, 142, 175), (2022, 'ALBERT SIDOISNE', 44, -45, 14), (2025, 'MATHURIN REGNIER', 13, 700, -452)],
                                             'ALBERT SIDOISNE', 44) is None #cas où les résultats correspondent au nom et département mais erreur entre nombre présents et admis (retourne None)
    assert dnb.moyenne_taux_reussite_college([(2011, 'ALBERT SIDOISNE', 25, 112, 102), (2012, 'ALBERT SIDOISNE', 86, 28, 26)],
                                             'ALBERT SIDOISNE', 21) is None #cas où le nom correpond mais pas le département
    assert dnb.moyenne_taux_reussite_college([(2000, 'MATHURIN REGNIER', 37, 95, 79), (2010, 'MATHURIN REGNIER', 37, 54, 52)],
                                             'ALBERT SIDOISNE', 37) is None #cas où le département correspond mais pas le nom


def test_meilleur_college():
    assert dnb.meilleur_college(dnb.liste1, 2018) == (None, None) #cas où y'a aucun résultat avec la session demandée
    assert dnb.meilleur_college(dnb.liste2, 2021) == ('JEAN MONNET', 28) #cas normal
    assert dnb.meilleur_college([(2020, 'ALBERT SIDOISNE', 44, 142, 175), (2020, 'ALBERT SIDOISNE', 44, -45, 14), (2020, 'MATHURIN REGNIER', 13, 700, -452)], 2020) == (None, None) #cas où les résultats 
    #sont de la session demandée mais ont une erreur (retourne None) car soit nb_admis supérieur au nb_présents soit nb_admis ou nb_présents sont négatifs
    assert dnb.meilleur_college([], 2018) == (None, None) #cas où la liste de résultats est vide
    assert dnb.meilleur_college([(2020, 'EUGENE NONNON', 41, 142, 140), (2020, 'ALBERT SIDOISNE', 44, 142, 140)], 2020) == ('ALBERT SIDOISNE', 44) #cas où les taux de réussite sont égaux
    assert dnb.meilleur_college([(2020, 'ALBERT SIDOISNE', 44, -45, 14), (2020, 'MATHURIN REGNIER', 13, 700, -452), (2020, 'HUBERT GAUDER', 81, 110, 94)], 2020) == ('HUBERT GAUDER', 81) #cas
    #où les taux de réussite non-valides sont oubliés pour garder que les valides


def test_liste_sessions():
    assert dnb.liste_sessions([]) == [] #cas où la liste de résultats est vide
    assert dnb.liste_sessions(dnb.liste2) == [2020, 2021] #cas normal (en enlevant les doublons)
    assert dnb.liste_sessions([(2012, "EUGENE NONNON", 45, 94, 91), (2012, "ALBERT HUGER", 13, 42, 13)]) == [2012] #cas où il n'y a qu'une session dans la liste de résultats (2012)
    assert dnb.liste_sessions([(2020, "ALBERT SIDOISNE", 18, 90, -26), (2025, 'ALBERT SIDOISNE', 14, 0, 118), (2031, 'ALBERT SIDOISNE', 14, 0, 118)]) == [2020, 2025, 2031] #cas où les résultats
    #sont tous de la même session (aucun doublon)


def test_plus_longue_periode_amelioration():
    assert dnb.plus_longue_periode_amelioration(dnb.liste5) == (2013, 2017) #cas normal
    assert dnb.plus_longue_periode_amelioration(dnb.liste1) == (2020, 2020) #cas où il n'y a qu'une session
    assert dnb.plus_longue_periode_amelioration([]) == (None, None) #cas où la liste de résultats est vide
    assert dnb.plus_longue_periode_amelioration([(2008, "JEANNE D'ARC", 45, 230, 188), (2009, "ALBERT SIDOISNE", 28, 115, 94), (2012, "JEAN MONNET", 37, 115, 94),
                                                         (2018, 'ALBERT SIDOISNE', 28, 460, 376), (2020, 'MATHURIN REGNIER', 28, 690, 564)]) == (2020, 2020) #cas où il n'y
    #a aucune amélioration (stagnation des taux de réussite)
    assert dnb.plus_longue_periode_amelioration([(2008, "JEANNE D'ARC", 45, 58, 56), (2009, "ALBERT SIDOISNE", 28, 25, 23), (2012, "JEAN MONNET", 37, 105, 94),
                                                         (2018, 'ALBERT SIDOISNE', 28, 58, 46), (2020, 'MATHURIN REGNIER', 28, 97, 25)]) == (2020, 2020) #cas où il n'y aucune amélioration
    #(baisse des taux de réussite)
    assert dnb.plus_longue_periode_amelioration([(2020, "EUGENE NONNON", 15, 12, 9)]) == (2020, 2020) #cas où il n'y qu'un résultat dans la liste de résultats
    assert dnb.plus_longue_periode_amelioration([(2008, "JEANNE D'ARC", 45, 97, 9), (2009, "ALBERT SIDOISNE", 28, 42, 19), (2012, "JEAN MONNET", 37, 132, 94),
                                                         (2018, 'ALBERT SIDOISNE', 28, 97, 82), (2020, 'MATHURIN REGNIER', 28, 84, 82)]) == (2008, 2020) #cas où la période d'amélioration
    #dure toute les sessions de la liste de résultats
    assert dnb.plus_longue_periode_amelioration([(2008, "JEANNE D'ARC", 45, 97, 9), (2009, "ALBERT SIDOISNE", 28, 42, -19), (2012, "JEAN MONNET", 37, 132, 94),
                                                         (2018, 'ALBERT SIDOISNE', 28, 97, 82), (2020, 'MATHURIN REGNIER', 28, 84, 95)]) == (2008, 2020) #cas où il y a des erreurs dans les
    #résultats (nb_admis négatif, nb_amdis supérieur au nb_presents)


def test_est_bien_triee():
    assert dnb.est_bien_triee(dnb.liste1) #cas normal
    assert dnb.est_bien_triee([]) #cas où la liste de résultats est vide
    assert dnb.est_bien_triee([(2014, 'ALBERT SIDOISNE', 21, 75, 43)]) #cas où la liste ne comporte qu'un seul résultat
    assert not dnb.est_bien_triee([(2022, 'ALBERT SIDOISNE', 44, -45, 14), (2022, 'HUBERT GAUDER', 47, 700, -452), (2008, 'MATHURIN REGNIER', 81, 110, 94)]) #cas où uniquement les sessions ne sont
    #pas triées dans l'ordre chronologique même que les noms et les départements soient bien triés comme on le souhaite
    assert not dnb.est_bien_triee([(2022, 'ALBERT SIDOISNE', 44, -45, 14), (2022, 'MATHURIN REGNIER', 44, 700, -452), (2022, 'HUBERT GAUDER', 44, 110, 94)]) #cas où les sessions sont égales et
    #les départements aussi mais les noms pas triés dans l'ordre alphabétique
    assert dnb.est_bien_triee([(2022, 'ALBERT SIDOISNE', 44, -45, 14), (2022, 'MATHURIN REGNIER', 44, 700, -452), (2022, 'HUBERT GAUDER', 45, 110, 94)]) #cas où les sessions sont égales,
    #les départements sont bien triés par ordre croissant mais les noms ne sont pas triés dans l'ordre alphabétique
    assert dnb.est_bien_triee([(2018, 'ALBERT SIDOISNE', 44, -45, 14), (2020, 'HUBERT GAUDER', 13, 700, -452), (2021, 'MATHURIN REGNIER', 42, 110, 94)]) #cas où les sessions sont bien triées
    #dans l'ordre chronologique mais les départements et noms ne sont pas bien triés comme on le veut
    assert dnb.est_bien_triee([(2009, 'ZOLA EMILE', 18, -45, 14), (2012, 'ALBERT SIDOISNE', 27, 700, -452), (2017, 'HUBERT GAUDER', 73, 110, 94)]) #cas où les sessions sont bien triées
    #dans l'ordre chronologique, les départements sont bien triés dans l'ordre croissant mais les noms des collèges ne sont pas triés dans l'ordre alphabétique
    assert dnb.est_bien_triee([(2020, 'DE NERMONT - CHATEAUDUN', 28, 134, 118), (2020, 'DE NERMONT - CHATEAUDUN', 28, 37, 34), (2020, 'DE NERMONT - CHATEAUDUN', 28, 71, 60)]) #cas où les sessions, les
    #départements et les noms sont bien triés comme on le désire


def test_fusionner_resultats():
    assert dnb.fusionner_resultats(dnb.liste1, dnb.liste2) == [(2020, 'ALBERT SIDOISNE', 28, 134, 118), (2020, 'ANATOLE FRANCE', 28, 63, 47), (2020, 'DE NERMONT - CHATEAUDUN', 28, 74, 60),
                                                               (2020, 'DE NERMONT - NOGENT', 28, 28, 27), (2020, 'EMILE ZOLA', 28, 103, 88), (2020, 'GILBERT COURTOIS', 28, 22, 18),
                                                               (2020, 'MATHURIN REGNIER', 28, 152, 118), (2021, 'DE BEAUMONT LES AUTELS', 28, 37, 34), (2021, 'DE NERMONT - CHATEAUDUN', 28, 71, 60),
                                                               (2021, 'EMILE ZOLA', 28, 96, 85), (2021, 'GILBERT COURTOIS', 28, 24, 15), (2021, 'JEAN MONNET', 28, 97, 91),
                                                               (2021, 'LA PAJOTTERIE', 28, 91, 72), (2021, 'ND - LA LOUPE', 28, 12, 9), (2021, 'PIERRE BROSSOLETTE', 28, 93, 70),
                                                               (2021, 'SULLY', 28, 14, 10)] #cas où il y présence de doublons (donc on les ajoute pas)
    assert dnb.fusionner_resultats(dnb.liste1, []) == dnb.liste1 #cas où la deuxième liste passée en paramètre est vide
    assert dnb.fusionner_resultats([], dnb.liste1) == dnb.liste1 #cas où la première liste passée en paramètre est vide
    assert dnb.fusionner_resultats([], []) == [] #cas où les deux listes passées en paramètres sont vides
    assert dnb.fusionner_resultats(dnb.liste1, [(2021, 'DE BEAUMONT LES AUTELS', 28, 37, 34), (2021, 'DE NERMONT - CHATEAUDUN', 28, 71, 60), (2021, 'EMILE ZOLA', 28, 96, 85),
                                                (2021, 'GILBERT COURTOIS', 28, 24, 15)]) == [(2020, 'ALBERT SIDOISNE', 28, 134, 118), (2020, 'ANATOLE FRANCE', 28, 63, 47),
                                                                                             (2020, 'DE NERMONT - CHATEAUDUN', 28, 74, 60), (2020, 'DE NERMONT - NOGENT', 28, 28, 27),
                                                                                             (2020, 'EMILE ZOLA', 28, 103, 88), (2020, 'GILBERT COURTOIS', 28, 22, 18),
                                                                                             (2020, 'MATHURIN REGNIER', 28, 152, 118), (2021, 'DE BEAUMONT LES AUTELS', 28, 37, 34),
                                                                                             (2021, 'DE NERMONT - CHATEAUDUN', 28, 71, 60), (2021, 'EMILE ZOLA', 28, 96, 85),
                                                                                             (2021, 'GILBERT COURTOIS', 28, 24, 15)] #cas où il n'y a pas de doublons (normal)


def test_classement_global_departement(): #fonction rajoutée
    assert dnb.classement_global_departement([], 24) == [] #cas où la liste de résultats est vide
    assert dnb.classement_global_departement(dnb.liste1, 45) == [] #cas où le département n'est pas reporté dans la liste de résultats
    assert dnb.classement_global_departement(dnb.liste1, 28) == [('DE NERMONT - NOGENT', 27/28*100), ('ALBERT SIDOISNE', 118/134*100), ('EMILE ZOLA', 88/103*100), ('GILBERT COURTOIS', 18/22*100),
                                                                 ('DE NERMONT - CHATEAUDUN', 60/74*100), ('MATHURIN REGNIER', 118/152*100), ('ANATOLE FRANCE', 47/63*100)] #cas normal (ici tous les
    #résultats sont du département recherché)
    assert dnb.classement_global_departement([(2020, 'ALBERT SIDOISNE', 28, 134, 118), (2020, 'ANATOLE FRANCE', 12, 63, 47), (2020, 'DE NERMONT - CHATEAUDUN', 28, 74, 60),
                                              (2020, 'DE NERMONT - NOGENT', 75, 28, 27), (2020, 'EMILE ZOLA', 28, 103, 88)], 28) == [('ALBERT SIDOISNE', 118/134*100), ('EMILE ZOLA', 88/103*100),
                                                                 ('DE NERMONT - CHATEAUDUN', 60/74*100)] #cas normal (mais tous les résultats ne proviennent pas du département recherché)
    assert dnb.classement_global_departement([(2020, 'ALBERT SIDOISNE', 28, 134, 118), (2020, 'ANATOLE FRANCE', 12, 63, 47), (2020, 'DE NERMONT - CHATEAUDUN', 28, 74, 60),
                                              (2020, 'DE NERMONT - NOGENT', 75, 28, -15), (2020, 'EMILE ZOLA', 28, 103, 145)], 28) == [('ALBERT SIDOISNE', 118/134*100),
                                                                                                                                      ('DE NERMONT - CHATEAUDUN', 60/74*100)] #cas où il y a au moins
    #une erreur (résultat négatif ou nb_admis supérieur au nb_présents)


def test_taux_moyen_college_periode(): #fonction rajoutée
    assert dnb.taux_moyen_college_periode([(2020, 'ALBERT SIDOISNE', 28, 134, 118), (2020, 'ALBERT SIDOISNE', 28, 63, 47), (2021, 'ALBERT SIDOISNE', 28, 71, 60),
                                           (2022, 'ALBERT SIDOISNE', 28, 91, 72), (2025, 'ALBERT SIDOISNE', 28, 12, 9)],
                                          'ALBERT SIDOISNE', 28, (2021, 2025)) == ((60/71*100)+(72/91*100)+(9/12*100))/3 #cas normal
    assert dnb.taux_moyen_college_periode([], 'ALBERT SIDOISNE', 28, (2021, 2025)) is None #cas où la liste de résultats est vide
    assert dnb.taux_moyen_college_periode([(2020, 'ALBERT SIDOISNE', 28, 134, 118), (2020, 'ALBERT SIDOISNE', 28, 63, 47), (2021, 'ALBERT SIDOISNE', 28, 71, 60),
                                           (2022, 'ALBERT SIDOISNE', 28, 91, 72), (2025, 'ALBERT SIDOISNE', 28, 12, 9)],
                                          'ANATOLE FRANCE', 28, (2021, 2025)) is None #cas où le nom du collège n'existe pas dans la liste de résultats
    assert dnb.taux_moyen_college_periode([(2020, 'ALBERT SIDOISNE', 28, 134, 118), (2020, 'ALBERT SIDOISNE', 28, 63, 47), (2021, 'ALBERT SIDOISNE', 28, 71, 60),
                                           (2022, 'ALBERT SIDOISNE', 28, 91, 72), (2025, 'ALBERT SIDOISNE', 28, 12, 9)],
                                          'ALBERT SIDOISNE', 41, (2021, 2025)) is None #cas où le département du collège recherché n'est jamais présent dans la liste de résultats
    assert dnb.taux_moyen_college_periode([(2020, 'ALBERT SIDOISNE', 28, 134, 118), (2020, 'ALBERT SIDOISNE', 28, 63, 47), (2021, 'ALBERT SIDOISNE', 28, 71, 60),
                                           (2022, 'ALBERT SIDOISNE', 28, 91, 72), (2025, 'ALBERT SIDOISNE', 28, 12, 9)],
                                          'ALBERT SIDOISNE', 41, (2008, 2010)) is None #cas où la période n'est jamais reportée dans la liste de résultats (période inférieur à la session minimale)
    assert dnb.taux_moyen_college_periode([(2020, 'ALBERT SIDOISNE', 28, 134, 118), (2020, 'ALBERT SIDOISNE', 28, 63, 47), (2021, 'ALBERT SIDOISNE', 28, 71, 60),
                                           (2022, 'ALBERT SIDOISNE', 28, 91, 72), (2025, 'ALBERT SIDOISNE', 28, 12, 9)],
                                          'ALBERT SIDOISNE', 28, (2025, 2025)) == 9/12*100 #cas où la période est sur une unique session (2025-2025)
    assert dnb.taux_moyen_college_periode([(2020, 'ALBERT SIDOISNE', 28, 134, 118), (2020, 'ALBERT SIDOISNE', 28, 63, 47), (2021, 'ALBERT SIDOISNE', 28, 71, 60),
                                           (2022, 'ALBERT SIDOISNE', 28, 91, 72), (2025, 'ALBERT SIDOISNE', 28, 12, 9)],
                                          'ALBERT SIDOISNE', 28, (2026, 2031)) is None #cas où la période n'est jamais reportée dans la liste de résultats (période supérieur à la session maximale)
    assert dnb.taux_moyen_college_periode([(2020, 'ALBERT SIDOISNE', 28, 134, 118), (2020, 'ALBERT SIDOISNE', 28, 63, 47), (2021, 'ALBERT SIDOISNE', 28, 71, 60),
                                           (2022, 'ALBERT SIDOISNE', 28, 91, 72), (2025, 'ALBERT SIDOISNE', 28, 12, 9)],
                                          'ALBERT SIDOISNE', 28, (2025, 2021)) is None #cas où la période est invalide (2025-2021)
    assert dnb.taux_moyen_college_periode([(2020, 'ALBERT SIDOISNE', 28, 134, 118), (2020, 'ALBERT SIDOISNE', 28, 63, 47), (2022, 'ALBERT SIDOISNE', 28, 71, 60)], 'ALBERT SIDOISNE', 28,
                                          (2020, 2025)) == ((118/134*100)+(47/63*100)+(60/71*100))/3 #cas où la période_fin dépasse la session maximum dans la liste de résultats


def test_colleges_en_baisse(): #fonction rajoutée
    assert dnb.colleges_en_baisse([(2020, 'ALBERT SIDOISNE', 28, 134, 118), (2021, 'ALBERT SIDOISNE', 28, 71, 60), (2021, 'ALBERT SIDOISNE', 31, 134, 118), (2021, 'EMILE ZOLA', 28, 71, 60),
                          (2021, 'HUBERT MARCHAIN', 28, 63, 47), (2022, 'ALBERT SIDOISNE', 28, 12, 9), (2022, 'ALBERT SIDOISNE', 29, 63, 47), (2022, 'ALBERT SIDOISNE', 31, 91, 72),
                          (2022, 'EMILE ZOLA', 28, 91, 87), (2022, 'VICTOR HUGO', 28, 12, 9)], 2) == [('ALBERT SIDOISNE', 28, -5.6), ('ALBERT SIDOISNE', 31, -5.1)] #cas normal
    assert dnb.colleges_en_baisse([], 2) == [] #cas où la liste de résultats est vide
    assert dnb.colleges_en_baisse([(2020, 'ALBERT SIDOISNE', 28, 134, 118), (2021, 'ALBERT SIDOISNE', 28, 71, 60), (2021, 'ALBERT SIDOISNE', 31, 134, 118), (2021, 'EMILE ZOLA', 28, 71, 60),
                          (2021, 'HUBERT MARCHAIN', 28, 63, 47), (2022, 'ALBERT SIDOISNE', 28, 12, 9), (2022, 'ALBERT SIDOISNE', 29, 63, 47), (2022, 'ALBERT SIDOISNE', 31, 91, 72),
                          (2022, 'EMILE ZOLA', 28, 91, 87), (2022, 'VICTOR HUGO', 28, 12, 9)], 6) == [('ALBERT SIDOISNE', 28, -6.3), ('ALBERT SIDOISNE', 31, -5.1)] #cas où les nb_sessions
    #dernières sessions ne peuvent pas exister
    assert dnb.colleges_en_baisse([(2020, 'ALBERT SIDOISNE', 28, 134, 118), (2021, 'ALBERT SIDOISNE', 28, 71, 60), (2021, 'ALBERT SIDOISNE', 33, 134, 118), (2021, 'EMILE ZOLA', 28, 71, 60),
                          (2021, 'HUBERT MARCHAIN', 28, 63, 47), (2022, 'ALBERT SIDOISNE', 91, 12, 9), (2022, 'ALBERT SIDOISNE', 29, 63, 47), (2022, 'ALBERT SIDOISNE', 31, 91, 72),
                          (2022, 'EMILE ZOLA', 76, 91, 87), (2022, 'VICTOR HUGO', 28, 12, 9)], 2) == [] #cas où chaque collège n'a pas eu d'évolution de son taux de réussite durant
    #les nb_sessions dernières sessions (un unique résultat par session)
    assert dnb.colleges_en_baisse([(2020, 'ALBERT SIDOISNE', 28, 134, 118), (2021, 'ALBERT SIDOISNE', 28, 48, 36), (2021, 'ALBERT SIDOISNE', 31, 364, 288), (2021, 'EMILE ZOLA', 28, 71, 60),
                          (2021, 'HUBERT MARCHAIN', 28, 63, 47), (2022, 'ALBERT SIDOISNE', 28, 12, 9), (2022, 'ALBERT SIDOISNE', 29, 63, 47), (2022, 'ALBERT SIDOISNE', 31, 91, 72),
                          (2022, 'EMILE ZOLA', 28, 91, 87), (2022, 'VICTOR HUGO', 28, 12, 9)], 2) == [] #cas où chaque collège a eu que des augmentations ou des stagnations de son taux de
    #réussite (pas de baisse) -> EMILE ZOLA A AUGMENTÉ, ALBERT SIDOISNE (28) A STAGNÉ ET PAREIL POUR ALBERT SIDOISNE (31), les autres ont été reportés qu'une fois donc stagnation
    assert dnb.colleges_en_baisse([(2020, 'ALBERT SIDOISNE', 28, 134, 118), (2021, 'ALBERT SIDOISNE', 28, 48, 36), (2021, 'ALBERT SIDOISNE', 31, 364, 288), (2021, 'EMILE ZOLA', 28, 71, 60),
                          (2021, 'HUBERT MARCHAIN', 28, 63, 47), (2022, 'ALBERT SIDOISNE', 28, 12, 9), (2022, 'ALBERT SIDOISNE', 29, 63, 47), (2022, 'ALBERT SIDOISNE', 31, 91, 72),
                          (2022, 'EMILE ZOLA', 28, 91, 87), (2022, 'VICTOR HUGO', 28, 12, 9)], 1) == [] #cas nb_sessions=1 et qu'il n'y a pas de baisse
    assert dnb.colleges_en_baisse([(2020, 'ALBERT SIDOISNE', 28, 134, 118), (2021, 'ALBERT SIDOISNE', 28, 48, 36), (2021, 'ALBERT SIDOISNE', 31, 364, 288), (2021, 'EMILE ZOLA', 28, 71, 60),
                          (2021, 'HUBERT MARCHAIN', 28, 63, 47), (2022, 'ALBERT SIDOISNE', 28, 12, 9), (2022, 'ALBERT SIDOISNE', 29, 63, 47), (2022, 'ALBERT SIDOISNE', 31, 91, 72),
                          (2022, 'EMILE ZOLA', 28, 91, 87), (2022, 'VICTOR HUGO', 28, 12, 9)], 0) == [] #cas nb_sessions=0
    assert dnb.colleges_en_baisse([(2020, 'ALBERT SIDOISNE', 28, 134, 118), (2021, 'ALBERT SIDOISNE', 28, 48, 36), (2021, 'ALBERT SIDOISNE', 31, 364, 288), (2021, 'EMILE ZOLA', 28, 71, 60),
                          (2021, 'HUBERT MARCHAIN', 28, 63, 47), (2022, 'ALBERT SIDOISNE', 28, 12, 9), (2022, 'ALBERT SIDOISNE', 28, 63, 47), (2022, 'ALBERT SIDOISNE', 38, 91, 72),
                          (2022, 'EMILE ZOLA', 28, 91, 87), (2022, 'VICTOR HUGO', 28, 12, 9)], -4) == [] #cas où le nb_sessions est négatif
    assert dnb.colleges_en_baisse([(2020, 'ALBERT SIDOISNE', 28, 134, 118), (2021, 'ALBERT SIDOISNE', 28, 48, 36), (2021, 'ALBERT SIDOISNE', 31, 364, 288), (2021, 'EMILE ZOLA', 28, 71, 60),
                          (2021, 'HUBERT MARCHAIN', 28, 63, 47), (2022, 'ALBERT SIDOISNE', 28, 12, 9), (2022, 'ALBERT SIDOISNE', 28, 63, 27), (2022, 'ALBERT SIDOISNE', 28, 91, 72),
                          (2022, 'EMILE ZOLA', 28, 91, 87), (2022, 'VICTOR HUGO', 28, 12, 9)], 1) == [] #cas nb_sessions=1 et qu'il y a au moins une baisse du taux de réussite sur la dernière session
    #A REVOIR LE DERNIER TEST CAR IL DEVRAIT RETOURNER ('ALBERT SIDOISNE', 28, -12.4 ou -12.5 normalement)
    assert dnb.colleges_en_baisse([(2020, 'ALBERT SIDOISNE', 28, 134, 118), (2021, 'ALBERT SIDOISNE', 28, 71, 60), (2021, 'ALBERT SIDOISNE', 31, 134, 118), (2021, 'EMILE ZOLA', 28, 71, 60),
                          (2021, 'HUBERT MARCHAIN', 28, 63, 47), (2022, 'ALBERT SIDOISNE', 28, 78, -45), (2022, 'ALBERT SIDOISNE', 29, 63, 86), (2022, 'ALBERT SIDOISNE', 31, 91, 72),
                          (2022, 'EMILE ZOLA', 28, 91, 87), (2022, 'VICTOR HUGO', 28, 12, 9)], 2) == [('ALBERT SIDOISNE', 31, -5.1)] #cas où il y a des erreurs de résultats (nb_admis négatifs et
    #nb_admis supérieur au nb_présents)