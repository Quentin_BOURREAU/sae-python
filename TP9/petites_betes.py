"""Init Dev : TP9"""


# ==========================
# Petites bêtes
# ==========================

def toutes_les_familles(pokedex):
    """détermine l'ensemble des familles représentées dans le pokedex

    Complexité de O(n)

    Args:
        pokedex (list): liste de pokemon, chaque pokemon est modélisé par
        un couple de str (nom, famille)

    Returns:
        set: l'ensemble des familles représentées dans le pokedex

    Invariant:
        familles contient l'ensemble des familles de pokémons parmi les pokémons du pokedex déjà traités
    """
    familles = set()
    for (_, famille) in pokedex:
        if famille not in familles:
            familles.add(famille)
    return familles


def nombre_pokemons(pokedex, famille):
    """calcule le nombre de pokemons d'une certaine famille dans un pokedex
    
    Complexité de O(n)

    Args:
        pokedex (list): liste de pokemon, chaque pokemon est modélisé par
        un couple de str (nom, famille)
        famille (str): le nom de la famille concernée

    Returns:
        int: le nombre de pokemons d'une certaine famille dans un pokedex
        
    Invariant:
        cpt_famille contient le nombre de pokémons appartenant à la famille désirée parmi les pokémons déjà traités
    """
    cpt_famille = 0
    for (_, famille_pok) in pokedex:
        if famille_pok == famille:
            cpt_famille += 1
    return cpt_famille

def frequences_famille(pokedex):
    """Construit le dictionnaire de fréqeunces des familles d'un pokedex
    
    Complexité de O(n)

    Args:
        pokedex (list): liste de pokemon, chaque pokemon est modélisé par
        un couple de str (nom, famille)

    Returns:
        dict: un dictionnaire dont les clés sont le nom de familles (str)
        et la valeur associée est le nombre de représentants de la famille (int)
    
    Invariant:
        dico contient les clés nom_famille avec comme valeurs les fréquences de pokémons de cette famille parmi les pokémons déjà traités
    """
    dico = {}
    for (_, famille) in pokedex:
        if famille not in dico:
            dico[famille] = 1
        else:
            dico[famille] += 1
    return dico

def dico_par_famille(pokedex):
    """Construit un dictionnaire dont les les clés sont le nom de familles (str)
    et la valeur associée est l'ensemble (set) des noms des pokemons de cette
    famille dans le pokedex
    
    Complexité de O(n)

    Args:
        pokedex (list): liste de pokemon, chaque pokemon est modélisé par
        un couple de str (nom, famille)

    Returns:
        dict: un dictionnaire dont les clés sont le nom de familles (str) et la valeur associée est
        l'ensemble (set) des noms des pokemons de cette famille dans le pokedex

    Invariant:
        dico contient les clés nom_famille avec comme valeurs l'ensemble des noms des pokémons de cette famille parmi les pokémons déjà traités
    """
    dico = {}
    for (nom_pokemon, famille) in pokedex:
        if famille not in dico:
            dico[famille] = {nom_pokemon}
        else:
            ensemble_famille = dico[famille]
            ensemble_famille.add(nom_pokemon)
            dico[famille] = ensemble_famille
    return dico


def famille_la_plus_representee(pokedex):
    """détermine le nom de la famille la plus représentée dans le pokedex
s
    Complexité de O(n)

    Args:
        pokedex (list): liste de pokemon, chaque pokemon est modélisé par
        un couple de str (nom, famille)

    Returns:
        str: le nom de la famille la plus représentée dans le pokedex

    Invariant:
        max_famille contient la fréquence maximum d'une famille parmi les familles déjà traitées
        nom_max_famille contient le nom de la famille qui a la fréquence maximum parmi les familles déjà traitées
    """
    dico = frequences_famille(pokedex)
    max_famille = 0
    nom_max_famille = None
    for (nom_famille, nb_famille) in dico.items():
        if nb_famille > max_famille:
            max_famille = nb_famille
            nom_max_famille = nom_famille
    return nom_max_famille

# ==========================
# Petites bêtes (la suite)
# ==========================


def toutes_les_familles_v2(pokedex):
    """détermine l'ensemble des familles représentées dans le pokedex
    
    Complexité de O(n**2)

    Args:
        pokedex (dict): un dictionnaire dont les clés sont les noms de pokemons et la
        valeur associée l'ensemble (set) de ses familles (str)

    Returns:
        set: l'ensemble des familles représentées dans le pokedex
        
    Invariant:
        ensemble_familles contient l'ensemble des familles de pokémons parmi les pokémons du pokedex déjà traités
    """
    ensemble_familles = set()
    for (_, familles_pok) in pokedex.items():
        for famille in familles_pok:
            if famille not in ensemble_familles:
                ensemble_familles.add(famille)
    return ensemble_familles

def nombre_pokemons_v2(pokedex, famille):
    """calcule le nombre de pokemons d'une certaine famille dans un pokedex
    
    Complexité de O(n**2)

    Args:
        pokedex (dict): un dictionnaire dont les clés sont les noms de pokemons et la
        valeur associée l'ensemble (set) de ses familles (str)
        famille (str): le nom de la famille concernée

    Returns:
        int: le nombre de pokemons d'une certaine famille dans un pokedex
        
    Invariant:
        cpt_famille contient le nombre de pokémons appartenant à la famille désirée parmi les pokémons déjà traités
    """
    cpt_famille = 0
    for (_, familles_pok) in pokedex.items():
        for famille_pok in familles_pok:
            if famille_pok == famille:
                cpt_famille += 1
    return cpt_famille

def frequences_famille_v2(pokedex):
    """Construit le dictionnaire de fréqeunces des familles d'un pokedex
    
    Complexité de O(n**2)

    Args:
        pokedex (dict): un dictionnaire dont les clés sont les noms de pokemons et la
        valeur associée l'ensemble (set) de ses familles (str)

    Returns:
        dict: un dictionnaire dont les clés sont le nom de familles (str) et la valeur
        associée est le nombre de représentants de la famille (int)
        
    Invariant:
        dico contient les clés nom_famille avec comme valeurs les fréquences de pokémons de cette famille parmi les pokémons déjà traités
    """
    dico = {}
    for (_, familles_pok) in pokedex.items():
        for famille_pok in familles_pok:
            if famille_pok not in dico:
                dico[famille_pok] = 1
            else:
                dico[famille_pok] += 1
    return dico

def dico_par_famille_v2(pokedex):
    """Construit un dictionnaire dont les les clés sont le nom de familles (str)
    et la valeur associée est l'ensemble (set) des noms des pokemons de
    cette famille dans le pokedex
    
    Complexité de O(n**2)

    Args:
        pokedex (dict): un dictionnaire dont les clés sont les noms de pokemons et la
        valeur associée l'ensemble (set) de ses familles (str)

    Returns:
        dict: un dictionnaire dont les clés sont le nom de familles (str) et la valeur associée est
        l'ensemble (set) des noms des pokemons de cette famille dans le pokedex
        
    Invariant:
        dico contient les clés nom_famille avec comme valeurs l'ensemble des noms des pokémons de cette famille parmi les pokémons déjà traités
    """
    dico = {}
    for (nom_pokemon, familles_pok) in pokedex.items():
        for famille_pok in familles_pok:
            if famille_pok not in dico:
                dico[famille_pok] = {nom_pokemon}
            else:
                ensemble_famille = dico[famille_pok]
                ensemble_famille.add(nom_pokemon)
                dico[famille_pok] = ensemble_famille
    return dico

def famille_la_plus_representee_v2(pokedex):
    """détermine le nom de la famille la plus représentée dans le pokedex
    
    Complexité de O(n)

    Args:
        pokedex (dict): un dictionnaire dont les clés sont les noms de pokemons et la
        valeur associée l'ensemble (set) de ses familles (str)

    Returns:
        str: le nom de la famille la plus représentée dans le pokedex
        
    Invariant:
        max_famille contient la fréquence maximum d'une famille parmi les familles déjà traitées
        nom_max_famille contient le nom de la famille qui a la fréquence maximum parmi les familles déjà traitées
    """
    dico = frequences_famille_v2(pokedex)
    max_famille = 0
    nom_max_famille = None
    for (nom_famille, nb_famille) in dico.items():
        if nb_famille > max_famille:
            max_famille = nb_famille
            nom_max_famille = nom_famille
    return nom_max_famille
