""" Matrices : API n 1 """



def construit_matrice(nb_lignes, nb_colonnes, valeur_par_defaut):
    """crée une nouvelle matrice en mettant la valeur par défaut dans chacune de ses cases.

    Args:
        nb_lignes (int): le nombre de lignes de la matrice
        nb_colonnes (int): le nombre de colonnes de la matrice
        valeur_par_defaut : La valeur que prendra chacun des éléments de la matrice

    Returns:
        une nouvelle matrice qui contient la valeur par défaut dans chacune de ses cases
    """
    return (nb_lignes, nb_colonnes, [valeur_par_defaut]*(nb_colonnes*nb_lignes))



def set_val(matrice, ligne, colonne, nouvelle_valeur):
    """permet de modifier la valeur de l'élément qui se trouve à la ligne et à la colonne
    spécifiées. Cet élément prend alors la valeur nouvelle_valeur

    Args:
        matrice : une matrice
        ligne (int) : le numéro d'une ligne (la numérotation commence à zéro)
        colonne (int) : le numéro d'une colonne (la numérotation commence à zéro)
        nouvelle_valeur : la nouvelle valeur que l'on veut mettre dans la case

    Returns:
        None
    """
    matrice[2][ligne*matrice[1]+colonne] = nouvelle_valeur


def get_nb_lignes(matrice):
    """permet de connaître le nombre de lignes d'une matrice

    Args:
        matrice : une matrice

    Returns:
        int : le nombre de lignes de la matrice
    """
    return matrice[0]


def get_nb_colonnes(matrice):
    """permet de connaître le nombre de colonnes d'une matrice

    Args:
        matrice : une matrice

    Returns:
        int : le nombre de colonnes de la matrice
    """
    return matrice[1]


def get_val(matrice, ligne, colonne):
    """permet de connaître la valeur de l'élément de la matrice dont on connaît
    le numéro de ligne et le numéro de colonne.

    Args:
        matrice : une matrice
        ligne (int) : le numéro d'une ligne (la numérotation commence à zéro)
        colonne (int) : le numéro d'une colonne (la numérotation commence à zéro)

    Returns:
        la valeur qui est dans la case située à la ligne et la colonne spécifiées
    """
    return matrice[2][ligne*matrice[1]+colonne]

# Fonctions pour l'affichage 

def affiche_ligne_separatrice(matrice, taille_cellule=4):
    """fonction auxilliaire qui permet d'afficher (dans le terminal)
    une ligne séparatrice

    Args:
        matrice : une matrice
        taille_cellule (int, optional): la taille d'une cellule. Defaults to 4.
    """
    print()
    for _ in range(get_nb_colonnes(matrice) + 1):
        print('-'*taille_cellule+'+', end='')
    print()


def affiche(matrice, taille_cellule=4):
    """permet d'afficher une matrice dans le terminal

    Args:
        matrice : une matrice
        taille_cellule (int, optional): la taille d'une cellule. Defaults to 4.
    """
    nb_colonnes = get_nb_colonnes(matrice)
    nb_lignes = get_nb_lignes(matrice)
    print(' '*taille_cellule+'|', end='')
    for i in range(nb_colonnes):
        print(str(i).center(taille_cellule) + '|', end='')
    affiche_ligne_separatrice(matrice, taille_cellule)
    for i in range(nb_lignes):
        print(str(i).rjust(taille_cellule) + '|', end='')
        for j in range(nb_colonnes):
            print(str(get_val(matrice, i, j)).rjust(taille_cellule) + '|', end='')
        affiche_ligne_separatrice(matrice, taille_cellule)
    print()


# Ajouter ici les fonctions supplémentaires, sans oublier de compléter le fichier
# tests_API_matrice.py avec des fonctions de tests

def charge_matrice_str(nom_fichier):
    """permet créer une matrice de str à partir d'un fichier CSV.

    Args:
        nom_fichier (str): le nom d'un fichier CSV (séparateur  ',')

    Returns:
        une matrice de str

    Invariant:
        toutes les lignes jusqu'à ligne ont été ajoutées à la matrice (en enlevant le retour chariot)
        toutes les valeurs de la matrice chargée jusqu'à valeur ont été ajoutées (change la valeur à la place de None) à la matrice finale (matrice_api)
    """
    matrice = []
    nb_lignes = 0
    fic = open(nom_fichier, 'r')
    for ligne in fic:
        ligne_matrice = ligne.split(',')
        ligne_matrice.pop(-1)
        nb_lignes += 1
        matrice += ligne_matrice
    nb_colonnes = len(ligne_matrice)
    fic.close()
    matrice_api = construit_matrice(nb_lignes, nb_colonnes, None)
    num_ligne = 0
    num_colonne = 0
    for valeur in matrice:
        if num_colonne >= nb_colonnes:
            num_colonne = 0
            num_ligne += 1
        set_val(matrice_api, num_ligne, num_colonne, valeur)
        num_colonne += 1
    return matrice_api


def sauve_matrice(matrice, nom_fichier):
    """permet sauvegarder une matrice dans un fichier CSV.
    Attention, avec cette fonction, on perd l'information sur le type des éléments

    Args:
        matrice : une matrice
        nom_fichier (str): le nom du fichier CSV que l'on veut créer (écraser)

    Returns:
        None

    Invariant:
        toutes les colonnes de la ligne actuelle (num_ligne) jusqu'à num_colonne ont été traitées
    """
    fic = open(nom_fichier, 'w')
    nb_lignes = get_nb_lignes(matrice)
    nb_colonnes = get_nb_colonnes(matrice)
    num_ligne = 0
    num_colonne = 0
    ligne_fic = ''
    while num_ligne < nb_lignes and num_colonne < nb_colonnes:
        valeur_a_ajoutee = get_val(matrice, num_ligne, num_colonne)
        ligne_fic += str(valeur_a_ajoutee)+","
        num_colonne += 1
        if num_colonne == nb_colonnes:
            num_ligne += 1
            ligne_fic += "\n"
            if num_ligne < nb_lignes:
                num_colonne = 0
            fic.write(ligne_fic)
            ligne_fic = ''
            
# Exercice 5

def get_ligne(matrice, ligne):
    """permet de connaître la ligne d'une matrice à partir de son numéro

    Args:
        matrice : une matrice

    Returns:
        (list) : la liste des valeurs de la ligne de la matrice que l'on veut
        
    Invariant:
        liste_ligne contient la liste de la ligne que l'on veut parmi les valeurs de cette ligne déjà traitées
    """
    liste_ligne = []
    portion = matrice[2][ligne*matrice[1]:ligne*matrice[1]+matrice[1]]
    for valeur in portion:
        liste_ligne.append(valeur)
    return liste_ligne

print(get_ligne())