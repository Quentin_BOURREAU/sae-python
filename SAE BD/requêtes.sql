select NomZoo
from Zoo
where Pays = 'France';


/* Les noms des aliments où le stock d'un aliment est presque vide (strictement inférieur à 400kg) */
select NomAliment, Stock
from Aliment
where Stock < 400;


/* tous les noms des animaux où l'espèce a une population estimée supérieure ou égale 100 000 */
select Animal.NomAnimal
from Animal
join Espece on Animal.IdEspece = Espece.IdEspece
where Espece.PopulationEstimee >= 100000;


/* tous les noms des animaux et les noms de l'espèce associée où l'espèce est situé en Afrique du Sud */
select Animal.NomAnimal, Espece.NomVulgaire
from Repartir
join ZoneGeographique on ZoneGeographique.CodeZone = Repartir.CodeZone
join Espece on Espece.IdEspece = Repartir.IdEspece
join Animal on Animal.IdEspece = Espece.IdEspece
where ZoneGeographique.CodeZone = 1; /* 1 = Afrique du Sud */


/* tous les noms des animaux avec son espèce et le nom de l'employé chargé de leur donner qui mangent des feuilles quotidiennement avec la quantité correspondante */
select Animal.NomAnimal, Espece.NomVulgaire, Manger.NomEmploye, Manger.QuantiteQuotidienne
from Manger
join Aliment on Aliment.NomAliment = Manger.NomAliment
join Espece on Espece.IdEspece = Manger.IdEspece
join Animal on Animal.IdEspece = Espece.IdEspece
where Manger.NomAliment = 'feuilles';


/* toutes les zones où la famille des Éléphantidés est présente */
select ZoneGeographique.Libellee, Espece.NomVulgaire
from Repartir
join ZoneGeographique on ZoneGeographique.CodeZone = Repartir.CodeZone
join Espece on Espece.IdEspece = Repartir.IdEspece
join Famille on Famille.NomFamille = Espece.NomFamille
where Famille.NomFamille = 'Éléphantidés';






/* VOIR COMMENT GÉRER LE STOCK DE DEUX ZOOS DIFFÉRENT CAR LA POUR L'INSTANT C'EST UN STOCK POUR TOUS LES ZOOS + VERIFIER LE MCD SUIVANT LES NOMS DES ENTITÉS ET ATTRIBUTS
+ UTILISER DES REQUETES PLUS POUSSEES GENRE asc, desc, group by, count, sum, distinct, sum, avg, min, max... */





/* Les noms des animaux avec leur espèce étant nés avant le 1er janvier 2015 (2015-01-01) */
select Animal.NomAnimal, Espece.NomVulgaire, Animal.DdN
from Animal
join Espece on Espece.IdEspece = Animal.IdEspece
where Animal.DdN < '2015-01-01' order by Animal.DdN asc;


/* Les noms des animaux étant présents dans un des zoos depuis plus de 10ans (depuis 2012) */
select Animal.NomAnimal, Zoo.NomZoo, Animal.DateArrivee
from Animal
join Emplacement on Emplacement.CodeEmplacement = Animal.CodeEmplacement
join Zoo on Zoo.IdZoo = Emplacement.IdZoo
where Animal.DateArrivee < '2012-01-01' order by Animal.DateArrivee desc;


/* Les noms des animaux avec leur espèce présents au zoo de Beauval */
select Animal.NomAnimal, Espece.NomVulgaire
from Animal
join Emplacement on Emplacement.CodeEmplacement = Animal.CodeEmplacement
join Espece on Espece.IdEspece = Animal.IdEspece
where Emplacement.IdZoo = 1; /* 1 = Beauval */


/* Les noms des animaux avec leur espèce présents au zoo de Beauval et vivant dans une cage */
select Animal.NomAnimal, Espece.NomVulgaire
from Animal
join Emplacement on Emplacement.CodeEmplacement = Animal.CodeEmplacement
join Espece on Espece.IdEspece = Animal.IdEspece
join TypeEmplacement on TypeEmplacement.CodeType = Emplacement.CodeType
where Emplacement.IdZoo = 1 and TypeEmplacement.Libelle = 'type enclos et sous-type cage';


/* Tous les noms des espèces (sans doublons), le CodeEmplacement et le nom du Zoo où ils vivent dans des emplacements de type zone */
select distinct Espece.NomVulgaire, Emplacement.CodeEmplacement, Zoo.NomZoo
from Espece
join Animal on Animal.IdEspece = Espece.IdEspece
join Emplacement on Emplacement.CodeEmplacement = Animal.CodeEmplacement
join Zoo on Zoo.IdZoo = Emplacement.IdZoo
join TypeEmplacement on TypeEmplacement.CodeType = Emplacement.CodeType
where TypeEmplacement.Libelle in ('type zone et sous-type savane', 'type zone et sous-type foret', 'type zone et sous-type marécage');


/* Tous les noms des espèces avec leur besoin total en nourriture journalier (besoin total trié par ordre croissant) */
select distinct Espece.NomVulgaire, Manger.NomAliment, sum(Manger.QuantiteQuotidienne)
from Manger
join Espece on Espece.IdEspece = Manger.IdEspece
group by Espece.NomVulgaire order by sum(Manger.QuantiteQuotidienne) asc;