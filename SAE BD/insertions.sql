insert into Zoo (IdZoo, NomZoo, Ville, Pays, Telephone, Email, NomResponsable) values
(1, 'Beauval', 'Saint-Aignan', 'France', 0254755000, 'beauval@gmail.com', 'Rodolphe Delord'),
(2, 'Haute-Touche', 'Azay-le-Ferron', 'France', 0254022040, 'haute-touche@gmail.com', 'Yann Locatelli'),
(3, 'Vallée-des-Singes', 'Romagne', 'France', 0549872020, 'vallee-des-singes@gmail.com', 'Emmanuel le Grelle'),
(4, 'Pairi Daiza', 'Brugelette', 'Belgique', 3268250850, 'info@pairidaiza.eu', 'Eric Domb');


insert into Safari (IdZoo, NomZoo, Ville, Pays, Telephone, Email, NomResponsable, NomSafari, Superficie, ModeLocomotion) values
(2, 'Haute-Touche', 'Azay-le-Ferron', 'France', 0254022040, 'haute-touche@gmail.com', 'Yann Locatelli', 'Safari Voiture', '436', 'voiture du Safari');


insert into Parc_zoologique (IdZoo, NomZoo, Ville, Pays, Telephone, Email, NomResponsable, NomParc, NomEquipe) values 
(1, 'Beauval', 'Saint-Aignan', 'France', 0254755000, 'beauval@gmail.com', 'Rodolphe Delord', 'ZooParc de Beauval', 'Équipe du ZooParc de Beauval'),
(3, 'Vallée-des-Singes', 'Romagne', 'France', 0549872020, 'vallee-des-singes@gmail.com', 'Emmanuel le Grelle', 'Parc de la Vallée des Singes', 'Équipe de la Vallée des Singes'),
(4, 'Pairi Daiza', 'Brugelette', 'Belgique', 3268250850, 'info@pairidaiza.eu', 'Eric Domb', 'Pairi Daiza', 'Équipe de Pairi Daiza');


/* A CHANGER LES NOMS DES ATTRIBUTS COMPOSÉS GENRE CodeType au lieu de Code_Type OU NomVulgaire au lieu de Nom_vulgaire (METTRE DES NOMS UNIVERSELS) */

insert into TypeEmplacement (CodeType, Libelle, Procedures) values 
(1, 'type enclos et sous-type cage', 'les conditions de vie des animaux doivent satisfaire ses besoins et permettre la sécurité des visiteurs. Les animaux doivent avoir un minimum de place dans la cage.'),
(2, 'type enclos et sous-type aquarium', 'respecter la biodiversité de ces animaux.'),
(3, 'type zone et sous-type savane', 'environnement respectueux de la biodiversité et veiller à que cela ne soit pas dangeureux pour les visites.'),
(4, 'type zone et sous-type foret', 'environnement respectueux de la biodiversité et laisser la biodiversité se développer dedans.'),
(5, 'type zone et sous-type marécage', 'donner de la nourriture avec une perche, en aucun cas rentrer dans la zone.');


insert into Emplacement (CodeEmplacement, IdZoo, Situation, CodeType) values 
('E1', 1, 'B1', 1), /* ca sera des lions dans une cage E1 du zoo de Beauval */
('Z1', 2, 'A1', 3), /* ca sera des éléphants, une girafe, un okapi et un rhinocéros blanc dans la savane Z1 du safari la Haute-Touche */
('E101', 4, 'Z1', 2), /* ca sera un requin dans un aquarium E101 de Pairi Daiza */
('E2', 1, 'B2', 1), /* ca sera des léopards dans une cage E2 du zoo de Beauval */
('Z2', 1, 'A5', 1), /* ca sera des éléphants dans une cage Z2 du zoo de Beauval */
('Z3', 4, 'C1', 5), /* ca sera des crocodiles des marais dans les marécages Z3 du zoo de Pairi Daiza */
('Z4', 4, 'C7', 5), /* ca sera un faux-gavial de Malaisie dans les marécages Z4 du zoo de Pairi Daiza */
('Z5', 1, 'D1', 3), /* ca sera un lynx dans la savane Z5 du zoo de Beauval */
('E3', 3, 'C1', 1); /* ca sera un gorille et des chimpanzés dans une cage E3 du zoo de la Vallée des Singes */


insert into Famille (NomFamille, DescriptionFamille) values 
('Éléphantidés', 'Ils ont une trompe incroyablement grande'),
('Sphyrnidae', 'Leur forme leur donne une meilleure vision, tant au niveau du champ visuel que de la vision stéréoscopique, mais également un odorat plus développé'),
('Félidés', 'Tous ont une très bonne vision nocturne, une ouïe excellente et un bon odorat. Ils sont en général de bons grimpeurs.'),
('Crocodylidae', 'Ce sont des reptiles ayant la forme de grands lézards, avec un corps robuste, un long museau plat et proéminent, une queue comprimée latéralement et des yeux, des oreilles et des narines sur le dessus de la tête.'),
('Giraffidae', 'Ils ont une longue langue de couleur foncée, des canines lobées et des cornes recouvertes de peau, appelées ossicones.'),
('Rhinocérotidés', 'Leurs pattes ressemblent à celles du tapir mais chaque pied a trois doigts se terminant chacun par un gros ongle comme 3 sabots miniatures. La peau est épaisse et de couleur grise ou brune.'),
('Hominidés', 'Ils sont les plus grands primates, avec un corps robuste et des avant-bras bien développés. Aucun hominidé n’a de queue, et aucun n’a de callosités ischiatiques.');


insert into Espece (IdEspece, NomScientifique, NomVulgaire, PopulationEstimee, NomFamille) values 
(1, 'Loxodonta Africana', 'Éléphant d’Afrique', 415000, 'Éléphantidés'),
(2, 'Sphyrna mokarran', 'Grand requin-marteau', 1200, 'Sphyrnidae'),
(3, 'Elephas maximus', 'Éléphant d’Asie', 50000, 'Éléphantidés'),
(4, 'Panthera pardus', 'Léopard', 12000, 'Félidés'),
(5, 'Pantera leo', 'Lion', '20000', 'Félidés'),
(6, 'Crocodylus palustris', 'Crocodile des marais', 7500, 'Crocodylidae'),
(7, 'Tomistominae', 'Faux-gavial de Malaisie', 2500, 'Crocodylidae'),
(8, 'Okapia johnstoni', 'Okapi', 10000, 'Giraffidae'),
(9, 'Giraffa camelopardali', 'Girafe', 125000, 'Giraffidae'),
(10, 'Lynx', 'Lynx', 1365, 'Félidés'),
(11, 'Ceratotherium simum', 'Rhinocéros blanc', 20000, 'Rhinocérotidés'),
(12, 'Gorilla', 'Gorille', 600, 'Hominidés'),
(13, 'Pan troglodytes', 'Chimpanzé', 500000, 'Hominidés');


insert into Animal (IdAnimal, NomAnimal, Sexe, DdN, DateArrivee, Remarques, CodeEmplacement, IdZoo, IdEspece) values 
(1, 'Sharpedo', 'mâle', '2018-12-14', '2019-02-01', 'Espèce menacée de disparition, il a été récupéré en Méditérrannée. Sharpedo est le requin le plus grand du zoo.', 'E101', 4, 2),
(2, 'Babar', 'mâle', '2022-03-12', '2022-03-12', 'L’espèce est menacée de disparition. Babar est le premier éléphant né dans ce zoo.', 'Z1', 2, 1),
(3, 'Olga', 'femelle', '2009-10-04', '2010-08-26', 'À son transfert, Olga était une éléphante très appeurée suite à une existence tourmentée par les braconniers.', 'Z2', 1, 3),
(4, 'Elmer', 'mâle', '2006-07-19', '2010-09-18', 'L’espèce est menacée de disparition. Elmer est très sociable avec les autres.', 'Z1', 2, 1),
(5, 'Lumphy', 'femelle', '2010-05-24', '2014-12-04', 'L’espèce est menacée de disparition. Lumphy est très sensible aux humains.', 'Z1', 2, 1),
(6, 'Dumbo', 'mâle', '2015-04-28', '2018-04-19', 'Dumbo est très joueur.', 'Z2', 1, 3),
(7, 'Nala', 'femelle', '2018-08-21', '2018-09-28', 'Nala court très vite, à une vitesse de 68km/h.', 'E1', 1, 5),
(8, 'Zira', 'femelle', '2020-08-17', '2021-10-24', 'Zira est la première et la seule léoparde du zoo.', 'E2', 1, 4),
(9, 'Kovu', 'mâle', '2020-08-14', '2020-08-18', 'Kovu a une crinière plus épaisse que la moyenne', 'E1', 1, 5),
(10, 'Aldo', 'mâle', '2019-04-18', '2020-10-18', 'Aldo est très peureux.', 'Z3', 4, 6),
(11, 'Spike', 'mâle', '2020-09-17', '2020-09-19', 'Spike est un prédateur très intelligent pour manger ses proies.', 'Z4', 4, 7),
(12, 'Krocky', 'mâle', '2018-10-15', '2021-11-28', 'Krocky a une très grande machôire.', 'Z3', 4, 6),
(13, 'Sophie', 'femelle', '2017-12-17', '2019-01-27', 'Sophie a un plus petit cou que la normale.', 'Z1', 2, 9),
(14, 'Osiris', 'femelle', '2013-04-13', '2019-08-28', 'Osiris ne mange pas de fruits mais mange beaucoup de feuilles.', 'Z1', 2, 8),
(15, 'Acinox', 'mâle', '2017-04-17', '2018-11-24', 'Acinox ne peut pas voir loin (environ 3 mètres maximum).', 'Z5', 1, 10),
(16, 'Rufus', 'mâle', '2014-12-24', '2015-01-14', 'Rufus pèse 2200kg.', 'Z1', 2, 11),
(17, 'Waldorf', 'mâle', '2020-11-25', '2021-08-14', 'Waldorf est très joueur avec son frère Owen.', 'E3', 3, 13),
(18, 'Owen', 'mâle', '2020-12-14', '2021-08-14', 'Owen ne mange pas beaucoup par rapport à son frère Waldorf.', 'E3', 3, 13),
(19, 'Edwin', 'mâle', '2015-04-17', '2015-05-25', 'Edwin est très calme et ne bouge pratiquement que pour manger.', 'E3', 3, 12);


insert into ZoneGeographique (CodeZone, Libellee) values 
(1, 'une région dans la savane en Afrique du Sud'),
(2, 'une mer avec beaucoup de diversités de poissons et notamment quelques requins'),
(3, 'une région dans la savane en Asie'),
(4, 'une région dans les marécages en Malaisie (Asie)'),
(5, 'une région dans les fôrets tropicales en Amérique du Sud'),
(6, 'une région dans les marécages en Inde'),
(7, 'une région au Congo (forêt équatoriale de l’Ituri)'),
(8, 'une région en Amérique du Nord');


insert into Repartir (IdEspece, CodeZone, EffectifZone) values
(1, 1, 13000), /* éléphant d'Afrique */
(2, 2, 30), /* grand requin-marteau */
(3, 3, 35000), /* éléphant d'Asie */
(4, 1, 4500), /* léopards */
(5, 1, 12000), /* lions */
(6, 6, 3500), /* crocodiles des marais */
(7, 4, 2100), /* faux-gavials de Malaisie */
(8, 7, 6500), /* okapi */
(9, 1, 45000), /* girafe */
(10, 8, 740), /* lynx */
(11, 1, 2400), /* rhinocéros blanc */
(12, 7, 550), /* gorille */
(13, 7, 350000); /* chimpanzés */


insert into Aliment (NomAliment, Stock) values 
('foin', 1350),
('avoine', 1100),
('carottes', 1740),
('crustacés', 1340),
('luzerne', 1400),
('poissons', 1150),
('oiseaux', 1550),
('lapins', 800),
('poulets', 370),
('reptiles', 950),
('plantes', 350),
('fruits', 850),
('lièvres', 400),
('chevreuils', 355),
('feuilles', 2600),
('graines', 1050);


insert into Manger (IdEspece, NomAliment, NomEmploye, QuantiteQuotidienne) values 
(1, 'foin', 'Hugo B.', 80), /* éléphant d'Afrique */
(1, 'avoine', 'Hugo B.', 10), /* éléphant d'Afrique */
(1, 'carottes', 'Hugo B.', 5), /* éléphant d'Afrique */
(2, 'crustacés', 'Leila H.', 40), /* requin-marteau */
(3, 'foin', 'Hugo B.', 65), /* éléphant d'Asie */
(3, 'avoine', 'Hugo B.', 20), /* éléphant d'Asie */
(4, 'reptiles', 'Albin D.', 3), /* léopard */
(5, 'lapins', 'Albin D.', 7), /* lion */
(6, 'reptiles', 'Kevin L.', 1), /* crocodile */
(6, 'oiseaux', 'Kevin L.', 1), /* crocodile */
(7, 'poissons', 'Kevin L.', 2), /* faux-gavial */
(8, 'feuilles', 'Hugo B.', 30), /* okapi */
(9, 'feuilles', 'Hugo B.', 40), /* girafe */
(9, 'luzerne', 'Hugo B.', 10), /* girafe */
(10, 'lièvres', 'Hugo B.', 2), /* lynx */
(11, 'feuilles', 'Albin D.', 70), /* rhinocéros blanc */
(12, 'feuilles', 'Yann P.', 10), /* gorille */
(12, 'luzerne', 'Yann P.', 3), /* gorille */
(13, 'graines', 'Yann P.', 2); /* chimpanzé */


insert into Substituer (NomAliment, NomAlimentSubstitution, TauxRemplacement) values 
('foin', 'luzerne', 0.9),
('avoine', 'carottes', 0.7),
('carottes', 'foin', 0.8),
('crustacés', 'poissons', 0.8),
('reptiles', 'poissons', 0.8),
('poissons', 'crustacés', 1.2),
('lapins', 'poulets', 0.7),
('feuilles', 'plantes', 0.9),
('feuilles', 'fruits', 0.8),
('luzerne', 'fruits', 1.3),
('graines', 'feuilles', 0.9),
('fruits', 'graines', 1.1),
('oiseaux', 'reptiles', 0.7),
('poulets', 'lapins', 1.3),
('lièvres', 'chevreuils', 0.6);