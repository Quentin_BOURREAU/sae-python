/* A CHERCHER ET METTRE DES CONTRAINTES EN PLUS GENRE -> not null, unique, ... */

create table Zoo(
    IdZoo Number(20),
    NomZoo Varchar2(50),
    Ville Varchar2(20),
    Pays Varchar2(20),
    Telephone Varchar2(11) unique,
    Email Varchar2(100),
    NomResponsable Varchar2(50) not null,
    constraint ZooKey primary key (IdZoo));


create table Safari(
    IdZoo Number(20),
    NomZoo Varchar2(50),
    Ville Varchar2(20),
    Pays Varchar2(20),
    Telephone Varchar2(11) unique,
    Email Varchar2(100),
    NomResponsable Varchar2(50) not null,
    NomSafari Varchar2(50), /* A REVOIR CAR C'EST PAREIL QUE NomZoo */
    Superficie Number(100) not null,
    ModeLocomotion Varchar2(20) default 'Voiture',
    constraint checkSuperficie check (Superficie > 0),
    constraint SafariKey primary key (IdZoo)); /* A REVOIR LA CLÉ ÉTRANGÈRE */


create table ParcZoologique(
    IdZoo Number(20),
    NomZoo Varchar2(30),
    Ville Varchar2(20),
    Pays Varchar2(20),
    Telephone Varchar2(11) unique,
    Email Varchar2(30),
    NomResponsable Varchar2(20) not null,
    NomParc Varchar2(30), /* PAREIL QUE Safari */
    NomEquipe Varchar2(30) not null,
    constraint ParcZoologiqueKey primary key (IdZoo)); /* A REVOIR LA CLÉ ÉTRANGÈRE */


create table TypeEmplacement(
    CodeType Number(20),
    Libelle Varchar2(250), /* A REVOIR SI ON CHANGE CET ATTRIBUT PAR type et sous-type */
    Procedures Varchar2(250),
    constraint TypeKey primary key (CodeType));


create table Emplacement(
    CodeEmplacement Varchar2(20),
    IdZoo Number(20),
    Situation Varchar2(250) unique,
    CodeType Number(20),
    constraint EmplacementKey primary key (IdZoo, CodeEmplacement),
    constraint ZooKey foreign key (IdZoo) references Zoo(IdZoo),
    constraint TypeKey foreign key (CodeType) references TypeEmplacement(CodeType));


create table Famille(
    NomFamille Varchar2(50),
    DescriptionFamille Varchar2(250),
    constraint FamilleKey primary key (NomFamille));


create table Espece(
    IdEspece Number(20),
    NomScientifique Varchar2(50) unique,
    NomVulgaire Varchar2(50) unique,
    PopulationEstimee Number(100) not null,
    NomFamille Varchar2(50),
    constraint CheckPop check (PopulationEstimee > 0),
    constraint EspeceKey primary key (IdEspece),
    constraint FamilleKey foreign key (NomFamille) references Famille(NomFamille));


create table Animal(
    IdAnimal Number(20),
    NomAnimal Varchar2(20),
    Sexe Varchar2(7) not null,
    DdN Date not null,
    DateArrivee Date not null,
    Remarques Varchar2(250),
    CodeEmplacement Varchar2(20),
    IdZoo Number(20),
    IdEspece Number(20),
    constraint CheckDates check (DdN <= DateArrivee),
    constraint AnimalKey primary key (IdAnimal),
    constraint EmplacementKey foreign key (IdZoo, CodeEmplacement) references Emplacement(IdZoo, CodeEmplacement),
    constraint EspeceKey foreign key (IdEspece) references Espece(IdEspece));


create table ZoneGeographique(
    CodeZone Number(20),
    Libellee Varchar2(250),
    constraint ZoneGeographiqueKey primary key (CodeZone));

create table Repartir(
    IdEspece Number(20),
    CodeZone Number(20),
    EffectifZone Number(100) not null,
    constraint CheckEffectif check (EffectifZone > 0),
    constraint IdEspeceKey foreign key (IdEspece) references Espece(IdEspece),
    constraint ZoneGeographiqueKey foreign key (CodeZone) references ZoneGeographique(CodeZone),
    constraint RepartirKey primary key (IdEspece, CodeZone));


create table Aliment(
    NomAliment Varchar2(30),
    Stock Number(250) not null,
    constraint CheckStock check (Stock > 0),
    constraint AlimentKey primary key (NomAliment));


create table Manger(
    IdEspece Number(20),
    NomAliment Varchar2(30),
    NomEmploye Varchar2(50),
    QuantiteQuotidienne Number(100) not null,
    constraint CheckQuantite check (QuantiteQuotidienne > 0),
    constraint MangerKey primary key (IdEspece, NomAliment),
    constraint IdEspeceKey2 foreign key (IdEspece) references Espece(IdEspece),
    constraint NomAlimentKey foreign key (NomAliment) references Aliment(NomAliment));


create table Substituer(
    NomAliment Varchar2(30),
    NomAlimentSubstitution Varchar2(30),
    TauxRemplacement Decimal(2, 1),
    constraint CheckTaux check (TauxRemplacement > 0),
    constraint SubstituerKey primary key (NomAliment, NomAlimentSubstitution),
    constraint AlimentKey foreign key (NomAliment) references Aliment(NomAliment),
    constraint AlimentSubstitutionKey foreign key (NomAlimentSubstitution) references Aliment(NomAliment));
