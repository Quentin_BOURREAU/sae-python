create table Zoo(
    IdZoo Int,
    NomZoo Varchar(50),
    Ville Varchar(20),
    Pays Varchar(20),
    Telephone Varchar(11),
    Email Varchar(100),
    NomResponsable Varchar(50),
    constraint ZooKey primary key (IdZoo));


create table Safari(
    IdZoo Int,
    NomZoo Varchar(50),
    Ville Varchar(20),
    Pays Varchar(20),
    Telephone Varchar(11),
    Email Varchar(100),
    NomResponsable Varchar(50),
    NomSafari Varchar(50),
    Superficie Int,
    ModeLocomotion Varchar(20),
    constraint SafariKey primary key (IdZoo));


create table ParcZoologique(
    IdZoo Int,
    NomZoo Varchar(30),
    Ville Varchar(20),
    Pays Varchar(20),
    Telephone Varchar(11),
    Email Varchar(30),
    NomResponsable Varchar(20),
    NomParc Varchar(30),
    NomEquipe Varchar(30),
    constraint ParcZoologiqueKey primary key (IdZoo));


create table TypeEmplacement(
    CodeType Int,
    Libelle Varchar(250),
    Procedures Varchar(250),
    constraint TypeKey primary key (CodeType));


create table Emplacement(
    CodeEmplacement Varchar(20),
    IdZoo Int,
    Situation Varchar(250),
    CodeType Int,
    constraint EmplacementKey primary key (IdZoo, CodeEmplacement),
    constraint ZooKey foreign key (IdZoo) references Zoo(IdZoo),
    constraint TypeKey foreign key (CodeType) references TypeEmplacement(CodeType));


create table Famille(
    NomFamille Varchar(50),
    DescriptionFamille Varchar(250),
    constraint FamilleKey primary key (NomFamille));


create table Espece(
    IdEspece Int,
    NomScientifique Varchar(50),
    NomVulgaire Varchar(50),
    PopulationEstimee Int,
    NomFamille Varchar(50),
    constraint EspeceKey primary key (IdEspece),
    constraint FamilleKey foreign key (NomFamille) references Famille(NomFamille));


/* ERREUR -> Can't create table `bd_zoos`.`Animal` (errno: 150 "Foreign key constraint is incorrectly formed") */
create table Animal(
    IdAnimal Int,
    NomAnimal Varchar(20),
    Sexe Varchar(7),
    DdN Date,
    DateArrivee Date,
    Remarques Varchar(250),
    CodeEmplacement Varchar(20),
    IdZoo Int,
    IdEspece Int,
    constraint AnimalKey primary key (IdAnimal),
    constraint EmplacementKey foreign key (IdZoo, CodeEmplacement) references Emplacement(IdZoo, CodeEmplacement),
    constraint EspeceKey foreign key (IdEspece) references Espece(IdEspece));


create table ZoneGeographique(
    CodeZone Int,
    Libellee Varchar(250),
    constraint ZoneGeographiqueKey primary key (CodeZone));

create table Repartir(
    IdEspece Int,
    CodeZone Int,
    EffectifZone Int,
    constraint IdEspeceKey foreign key (IdEspece) references Espece(IdEspece),
    constraint ZoneGeographiqueKey foreign key (CodeZone) references ZoneGeographique(CodeZone),
    constraint RepartirKey primary key (IdEspece, CodeZone));


create table Aliment(
    NomAliment Varchar(30),
    Stock Int,
    constraint AlimentKey primary key (NomAliment));


/* ERREUR -> Can't create table `bd_zoos`.`Manger` (errno: 121 "Duplicate key on write or update") */
create table Manger(
    IdEspece Int,
    NomAliment Varchar(30),
    NomEmploye Varchar(50),
    QuantiteQuotidienne Int,
    constraint MangerKey primary key (IdEspece, NomAliment),
    constraint IdEspeceKey2 foreign key (IdEspece) references Espece(IdEspece),
    constraint NomAlimentKey foreign key (NomAliment) references Aliment(NomAliment));


create table Substituer(
    NomAliment Varchar(30),
    NomAlimentSubstitution Varchar(30),
    TauxRemplacement Decimal(2,1), /* A REVOIR CA FAIT BIEN UN FLOAT */
    constraint SubstituerKey primary key (NomAliment, NomAlimentSubstitution),
    constraint AlimentKey foreign key (NomAliment) references Aliment(NomAliment),
    constraint AlimentSubstitutionKey foreign key (NomAlimentSubstitution) references Aliment(NomAliment));