insert into Zoo (IdZoo, NomZoo, Ville, Pays, Telephone, Email, NomResponsable) values
(1, 'Beauval', 'Saint-Aignan', 'France', 0254755000, 'beauval@gmail.com', 'Rodolphe Delord'),
(2, 'Haute-Touche', 'Azay-le-Ferron', 'France', 0254022040, 'haute-touche@gmail.com', 'Yann Locatelli'),
(3, 'Vallée-des-Singes', 'Romagne', 'France', 0549872020, 'vallee-des-singes@gmail.com', 'Emmanuel le Grelle'),
(4, 'Pairi Daiza', 'Brugelette', 'Belgique', 3268250850, 'info@pairidaiza.eu', 'Eric Domb');


insert into Safari (IdZoo, NomZoo, Ville, Pays, Telephone, Email, NomResponsable, NomSafari, Superficie, ModeLocomotion) values
(2, 'Haute-Touche', 'Azay-le-Ferron', 'France', 0254022040, 'haute-touche@gmail.com', 'Yann Locatelli', 'Safari Voiture', '436', 'voiture ou à pied');


insert into Parc_zoologique (IdZoo, NomZoo, Ville, Pays, Telephone, Email, NomResponsable, NomParc, NomEquipe) values 
(1, 'Beauval', 'Saint-Aignan', 'France', 0254755000, 'beauval@gmail.com', 'Rodolphe Delord', 'ZooParc de Beauval', 'Équipe du ZooParc de Beauval'),
(3, 'Vallée-des-Singes', 'Romagne', 'France', 0549872020, 'vallee-des-singes@gmail.com', 'Emmanuel le Grelle', 'Parc de la Vallée des Singes', 'Équipe de la Vallée des Singes'),
(4, 'Pairi Daiza', 'Brugelette', 'Belgique', 3268250850, 'info@pairidaiza.eu', 'Eric Domb', 'Pairi Daiza', 'Équipe de Pairi Daiza');


/* A CHANGER LES NOMS DES ATTRIBUTS COMPOSÉS GENRE CodeType au lieu de Code_Type OU NomVulgaire au lieu de Nom_vulgaire (METTRE DES NOMS UNIVERSELS) */

insert into TypeEmplacement (CodeType, Libelle, Procedures) values 
(1, 'type enclos et sous-type cage', 'les conditions de vie des animaux doivent satisfaire ses besoins et permettre la sécurité des visiteurs. Les animaux doivent avoir un minimum de place dans la cage.'),
(2, 'type enclos et sous-type aquarium', 'respecter la biodiversité de ces animaux.'),
(3, 'type zone et sous-type savane', 'environnement respectueux de la biodiversité et veiller à que cela ne soit pas dangeureux pour les visites.'),
(4, 'type zone et sous-type foret', 'environnement respectueux de la biodiversité et laisser la biodiversité se développer dedans.');


insert into Emplacement (CodeEmplacement, IdZoo, Situation, CodeType) values 
('E1', 1, 'B1', 1), /* ca sera des lions dans une cage E1 du zoo de Beauval */
('Z1', 2, 'A1', 3), /* ca sera des girafes et éléphants dans la savane Z1 du safari la Haute-Touche */
('E101', 4, 'Z1', 2), /* ca sera des requins dans un aquarium E101 de Pairi Daiza */
('E2', 1, 'B2', 1), /* ca sera des lions dans une cage E2 du zoo de Beauval */
('E3', 1, 'B3', 1); /* ca sera des lions dans une cage E3 du zoo de Beauval */


insert into Famille (NomFamille, DescriptionFamille) values 
('Éléphantidés', 'Ils ont une trompe incroyablement grande'),
('Sphyrnidae', 'Leur forme leur donne une meilleure vision, tant au niveau du champ visuel que de la vision stéréoscopique, mais également un odorat plus développé'),
('Félidés', 'Tous ont une très bonne vision nocturne, une ouïe excellente et un bon odorat. Ils sont en général de bons grimpeurs.'),
('Crocodylidae', 'Ce sont des reptiles ayant la forme de grands lézards, avec un corps robuste, un long museau plat et proéminent, une queue comprimée latéralement et des yeux, des oreilles et des narines sur le dessus de la tête.');


insert into Espece (IdEspece, NomScientifique, NomVulgaire, PopulationEstimee, NomFamille) values 
(1, 'Loxodonta Africana', 'Éléphant d’Afrique', 415000, 'Éléphantidés'),
(2, 'Sphyrna mokarran', 'Grand requin-marteau', 1200, 'Sphyrnidae'),
(3, 'Elephas maximus', 'Éléphant d’Asie', 50000, 'Éléphantidés'),
(4, 'Panthera pardus', 'Léopard', 12000, 'Félidés'),
(5, 'Pantera leo', 'Lion', '20000', 'Félidés'),
(6, 'Crocodylus palustris', 'Crocodiles des marais', 7500, 'Crocodylidae'),
(7, 'Tomistominae', 'Faux-gavial de Malaisie', 2500, 'Crocodylidae');


insert into Animal (IdAnimal, NomAnimal, Sexe, DdN, DateArrivee, Remarques, CodeEmplacement, IdEspece) values 
(1, 'Sharpedo', 'mâle', 14/12/2018, 01/02/2019, 'Espèce menacée de disparition, il a été récupéré en Méditérrannée. Sharpedo est le requin le plus grand du zoo.', 'E101', 2),
(2, 'Babar', 'mâle', 12/03/2022, 12/03/2022, 'L’espèce est menacée de disparition. Babar est le premier éléphant né dans ce zoo.', 'Z1', 1);


insert into ZoneGeographique (CodeZone, Libellee) values 
(1, 'une région dans la savane en Afrique du Sud'),
(2, 'une mer avec beaucoup de diversités de poissons et notamment quelques requins');


insert into Repartir (IdEspece, CodeZone, EffectifZone) values 
(1, 1, 13000),
(2, 2, 30);

insert into Aliment (NomAliment, Stock) values 
('foin', 1250),
('avoine', 1300),
('carottes', 2040),
('crustacés', 1840),
('luzerne', 1800),
('banane', 940),
('poissons', 1400);


insert into Manger (IdEspece, NomAliment, NomEmploye, QuantiteQuotidienne) values 
(1, 'foin', 'Hugo B.', 80),
(2, 'crustacés', 'Leila H.', 40);


insert into Substituer (NomAliment, NomAlimentSubstitution, TauxRemplacement) values 
('foin', 'luzerne', 0.9),
('crustacés', 'poissons', 0.8);