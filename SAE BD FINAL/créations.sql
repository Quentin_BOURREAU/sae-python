drop table Substituer;
drop table Manger;
drop table Aliment;
drop table Repartir;
drop table ZoneGeographique;
drop table Animal;
drop table Espece;
drop table Famille;
drop table Emplacement;
drop table TypeEmplacement;
drop table ParcZoologique;
drop table Safari;
drop table Zoo;

/* REMETTRE Varchar2 AU LIEU DE Varchar */

create table Zoo(
    IdZoo Int,
    NomZoo Varchar(50),
    Ville Varchar(20),
    Pays Varchar(20),
    Telephone Varchar(11) unique,
    Email Varchar(100),
    NomResponsable Varchar(50) not null,
    constraint ZooKey primary key (IdZoo));


create table Safari(
    IdZoo Int,
    NomZoo Varchar(50),
    Ville Varchar(20),
    Pays Varchar(20),
    Telephone Varchar(11) unique,
    Email Varchar(100),
    NomResponsable Varchar(50) not null,
    Superficie Int not null,
    ModeLocomotion Varchar(20) default 'Voiture',
    constraint checkSuperficie check (Superficie > 0),
    constraint SafariKey primary key (IdZoo)); /* A REVOIR LA CLÉ ÉTRANGÈRE */


create table ParcZoologique(
    IdZoo Int,
    NomZoo Varchar(30),
    Ville Varchar(20),
    Pays Varchar(20),
    Telephone Varchar(11) unique,
    Email Varchar(30),
    NomResponsable Varchar(20) not null,
    NomEquipe Varchar(30) not null,
    constraint ParcZoologiqueKey primary key (IdZoo)); /* A REVOIR LA CLÉ ÉTRANGÈRE */


create table TypeEmplacement(
    CodeType Int,
    Libelle Varchar(250), /* A REVOIR SI ON CHANGE CET ATTRIBUT PAR type et sous-type */
    Procedures Varchar(250),
    constraint TypeKey primary key (CodeType));


create table Emplacement(
    CodeEmplacement Varchar(20),
    IdZoo Int,
    Situation Varchar(250),
    CodeType Int,
    constraint EmplacementKey primary key (IdZoo, CodeEmplacement),
    constraint ZooKey foreign key (IdZoo) references Zoo(IdZoo),
    constraint TypeKey foreign key (CodeType) references TypeEmplacement(CodeType));


create table Famille(
    NomFamille Varchar(50),
    DescriptionFamille Varchar(250),
    constraint FamilleKey primary key (NomFamille));


create table Espece(
    IdEspece Int,
    NomScientifique Varchar(50) unique,
    NomVulgaire Varchar(50) unique,
    PopulationEstimee Int not null,
    NomFamille Varchar(50),
    constraint CheckPop check (PopulationEstimee > 0),
    constraint EspeceKey primary key (IdEspece),
    constraint FamilleKey foreign key (NomFamille) references Famille(NomFamille));


create table Animal(
    IdAnimal Int,
    NomAnimal Varchar(20),
    Sexe Varchar(7) not null,
    DdN Date not null,
    DateArrivee Date not null,
    Remarques Varchar(250),
    CodeEmplacement Varchar(20),
    IdZoo Int,
    IdEspece Int,
    constraint CheckDates check (DdN <= DateArrivee),
    constraint AnimalKey primary key (IdAnimal),
    constraint EmplacementKey foreign key (IdZoo, CodeEmplacement) references Emplacement(IdZoo, CodeEmplacement),
    constraint EspeceKey foreign key (IdEspece) references Espece(IdEspece));


create table ZoneGeographique(
    CodeZone Int,
    Libellee Varchar(250),
    constraint ZoneGeographiqueKey primary key (CodeZone));

create table Repartir(
    IdEspece Int,
    CodeZone Int,
    EffectifZone Int not null,
    constraint CheckEffectif check (EffectifZone > 0),
    constraint IdEspeceKey foreign key (IdEspece) references Espece(IdEspece),
    constraint ZoneGeographiqueKey foreign key (CodeZone) references ZoneGeographique(CodeZone),
    constraint RepartirKey primary key (IdEspece, CodeZone));


create table Aliment(
    NomAliment Varchar(30),
    Stock Int not null,
    constraint CheckStock check (Stock > 0),
    constraint AlimentKey primary key (NomAliment));


create table Manger(
    IdEspece Int,
    NomAliment Varchar(30),
    NomEmploye Varchar(50),
    QuantiteQuotidienne Int not null,
    constraint CheckQuantite check (QuantiteQuotidienne > 0),
    constraint MangerKey primary key (IdEspece, NomAliment),
    constraint IdEspeceKey2 foreign key (IdEspece) references Espece(IdEspece),
    constraint NomAlimentKey foreign key (NomAliment) references Aliment(NomAliment));


create table Substituer(
    NomAliment Varchar(30),
    NomAlimentSubstitution Varchar(30),
    TauxRemplacement Decimal(2, 1),
    constraint CheckTaux check (TauxRemplacement > 0),
    constraint SubstituerKey primary key (NomAliment, NomAlimentSubstitution),
    constraint AlimentKey foreign key (NomAliment) references Aliment(NomAliment),
    constraint AlimentSubstitutionKey foreign key (NomAlimentSubstitution) references Aliment(NomAliment));
