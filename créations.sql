/* A CHERCHER ET METTRE DES CONTRAINTES EN PLUS GENRE -> not null, unique, ... */

create table Zoo(
    IdZoo Number(20),
    NomZoo Varchar2(50),
    Ville Varchar2(20),
    Pays Varchar2(20),
    Telephone Varchar2(11),
    Email Varchar2(100),
    NomResponsable Varchar2(50),
    constraint ZooKey primary key (IdZoo));


create table Safari(
    IdZoo Number(20),
    NomZoo Varchar2(50),
    Ville Varchar2(20),
    Pays Varchar2(20),
    Telephone Varchar2(11),
    Email Varchar2(100),
    NomResponsable Varchar2(50),
    NomSafari Varchar2(50), /* A REVOIR CAR C'EST PAREIL QUE NomZoo */
    Superficie Number(100) not null,
    ModeLocomotion Varchar2(20),
    constraint SafariKey primary key (IdZoo)); /* A REVOIR LA CLÉ ÉTRANGÈRE */


create table ParcZoologique(
    IdZoo Number(20),
    NomZoo Varchar2(30),
    Ville Varchar2(20),
    Pays Varchar2(20),
    Telephone Varchar2(11),
    Email Varchar2(30),
    NomResponsable Varchar2(20),
    NomParc Varchar2(30), /* PAREIL QUE Safari */
    NomEquipe Varchar2(30),
    constraint ParcZoologiqueKey primary key (IdZoo)); /* A REVOIR LA CLÉ ÉTRANGÈRE */


create table TypeEmplacement(
    CodeType Number(20),
    Libelle Varchar2(250),
    Procedures Varchar2(250),
    constraint TypeKey primary key (CodeType));


create table Emplacement(
    CodeEmplacement Varchar2(20),
    IdZoo Number(20),
    Situation Varchar2(250),
    CodeType Number(20),
    constraint EmplacementKey primary key (IdZoo, CodeEmplacement),
    constraint IdZooKey foreign key (IdZoo) references Zoo(IdZoo),
    constraint CodeTypeKey foreign key (CodeType) references TypeEmplacement(CodeType));


create table Famille(
    NomFamille Varchar2(50),
    DescriptionFamille Varchar2(250),
    constraint FamilleKey primary key (NomFamille));


create table Espece(
    IdEspece Number(20),
    NomScientifique Varchar2(50),
    NomVulgaire Varchar2(50),
    PopulationEstimee Number(100) not null,
    NomFamille Varchar2(50),
    constraint EspeceKey primary key (IdEspece),
    constraint NomFamilleKey foreign key (NomFamille) references Famille(NomFamille));


create table Animal(
    IdAnimal Number(20),
    NomAnimal Varchar2(20),
    Sexe Varchar2(7),
    DdN Date,
    DateArrivee Date,
    Remarques Varchar2(250),
    CodeEmplacement Varchar2(20),
    IdEspece Number(20),
    constraint AnimalKey primary key (IdAnimal),
    constraint CodeEmplacementKey foreign key (CodeEmplacement) references Emplacement(CodeEmplacement),
    constraint IdEspeceKey foreign key (IdEspece) references Espece(IdEspece));


create table ZoneGeographique(
    CodeZone Number(20),
    Libellee Varchar2(250),
    constraint ZoneGeographiqueKey primary key (CodeZone));

create table Repartir(
    IdEspece Number(20),
    CodeZone Number(20),
    EffectifZone Number(100) not null,
    constraint RepartirKey primary key (IdEspece, CodeZone),
    constraint IdEspeceKey foreign key (IdEspece) references Espece(IdEspece),
    constraint CodeZoneKey foreign key (CodeZone) references ZoneGeographique(CodeZone));


create table Aliment(
    NomAliment Varchar2(30),
    Stock Number(250) not null,
    constraint AlimentKey primary key (NomAliment));


create table Manger(
    IdEspece Number(20),
    NomAliment Varchar2(30),
    NomEmploye Varchar2(50),
    QuantiteQuotidienne Number(100),
    constraint MangerKey primary key (IdEspece, NomAliment),
    constraint IdEspeceKey foreign key (IdEspece) references Espece(IdEspece),
    constraint NomAlimentKey foreign key (NomAliment) references Aliment(NomAliment));


create table Substituer(
    NomAliment Varchar2(30),
    NomAlimentSubstitution Varchar2(30),
    TauxRemplacement Number(10), /* A REVOIR SI ON MET UN FLOAT (0.9 au lieu de 90) */
    constraint SubstituerKey primary key (NomAliment, NomAlimentSubstitution),
    constraint NomAlimentKey foreign key (NomAliment) references Aliment(NomAliment),
    constraint NomAlimentSubstitutionKey foreign key (NomAlimentSubstitution) references Aliment(NomAliment));
